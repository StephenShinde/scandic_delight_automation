package com.scandic.Listners;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.scandic.ExternalUtilities.Log;



public class ITestListenerImpl implements ITestListener {
	
	

	@Override
	public void onFinish(ITestContext context) {
		Log.info("Regression suite is ending");
		
	}

	@Override
	public void onStart(ITestContext context) {
		Log.info("Regression suite is starting");
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		
		
	}

	@Override
	public void onTestFailedWithTimeout(ITestResult result) {
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		
	}
		

	@Override
	public void onTestSkipped(ITestResult result) {
		
		
	}

	@Override
	public void onTestStart(ITestResult result) {
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		
	}

}
