package com.scandic.base;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.scandic.Testutilities.MobileActions;
import com.scandic.Testutilities.Utility;
import com.scandic.pages.Launch_Page;
import com.scandic.pages.Login_Page;
import com.scandic.pages.My_Profile_Page;
import com.scandic.pages.Search_Hotel_Page;
import com.scandic.pages.Search_Hotel_Result_Page;
import com.scandic.pages.Signup_Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;


public class CreateDriver {

	private AppiumDriver<MobileElement> driver;
	//protected BookingPage b
	
	public ExtentReports extent;
	public static ExtentTest ScenarioDef;
	public static ExtentTest features;
	public static String reportLocation="";


	public static ThreadLocal<AppiumDriver> dr=new ThreadLocal<>();

	public AppiumDriver<MobileElement> getDriver() {
		return dr.get();
	}

	public void setDriver(AppiumDriver<MobileElement> driver) {
		dr.set(driver);
	}

	/**
	 * This function is used to setup capabilities
	 * @param platform_name
	 * @param version
	 * @param deviceName
	 * @param UDID
	 * @param port
	 * @param app
	 * @param automation_Name
	 * @throws IOException
	 * @throws Exception
	 */
	public void setUp (String platform_name,String version, String deviceName, String UDID, int port, String app, String automation_Name) throws IOException, Exception
	
	{

		String projectpath=System.getProperty("user.dir");
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(MobileCapabilityType.PLATFORM_NAME, platform_name);
		capability.setCapability(MobileCapabilityType.PLATFORM_VERSION, version);
		capability.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
		capability.setCapability(MobileCapabilityType.UDID, UDID);
		capability.setCapability(MobileCapabilityType.AUTOMATION_NAME, automation_Name);
		
//		capability.setCapability("appActivity", "app.scandichotels.com.mvp.splash.SplashActivity");
//		capability.setCapability(MobileCapabilityType.APP, projectpath+"/buildPath/"+app);
		
		capability.setCapability(MobileCapabilityType.APP, app);
		setDriver(new AppiumDriver<MobileElement>(new URL("http://127.0.0.1:"+port+"/wd/hub"),capability));
		getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		
	}

	/**
	 * Dispose driver object
	 */
	public void destroyApp() {
		getDriver().quit();
	}

}
