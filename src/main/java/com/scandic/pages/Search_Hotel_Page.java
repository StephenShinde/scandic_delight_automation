package com.scandic.pages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import com.scandic.Testutilities.TestUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Search_Hotel_Page {

	public Search_Hotel_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@iOSXCUITFindBy(id="Book Hotel_tab")
	@AndroidFindBy(id = "action_book")
	public MobileElement Book_Hotel_Menu;

	@iOSXCUITFindBy(id="search_button")
	@AndroidFindBy(id = "tv_book_hotel_text")
	public MobileElement Search_Hotel_Destination;
	
	@iOSXCUITFindBy(id="search_textField")
	@AndroidFindBy(id = "et_search_input")
	public MobileElement Search_text;

	@iOSXCUITFindBy(id="locationcell_item")
	@AndroidFindBy(id = "tv_name")
	public List <MobileElement> Search_Result_list;
	
	@iOSXCUITFindBy(id="search_button")
	@AndroidFindBy(id = "tv_book_hotel_text")
	public MobileElement Search_Hotel_Offer;
	

	
	public void clickonSearchHotelDestination() {
		Search_Hotel_Destination.click();
	}
	
	public void enterCityName() {
		TestUtil testutil=new TestUtil();
		Search_text.sendKeys(testutil.searchbycityname());
	}
	
	public void enterHotelName() {
		TestUtil testutil=new TestUtil();
		Search_text.sendKeys(testutil.searchbyHotelname());
	}
	
	public String getlistname() {
		return Search_Result_list.get(0).getText();
	}
	
	public void selectsearchlistfromlist() {
		Search_Result_list.get(0).click();
	
	}

	public void entercitynameSF(String cityName){
		Search_text.sendKeys(cityName);
	}
	
	
}
