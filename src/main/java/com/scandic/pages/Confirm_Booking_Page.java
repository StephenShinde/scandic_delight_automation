package com.scandic.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.scandic.base.CreateDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Confirm_Booking_Page extends CreateDriver{
	public Confirm_Booking_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@iOSXCUITFindBy(id="Confirm")
	@AndroidFindBy(id = "tv_flex_confirm")
	public MobileElement flex_confirm_header;
	
	@iOSXCUITFindBy(id="Done!")
	@AndroidFindBy(id = "tv_flex_confirm")
	public MobileElement flex_done_header;
	
	@iOSXCUITFindBy(id="Confirm")
	@AndroidFindBy(id = "tv_confirm")
	public MobileElement save_confirm_header;
	
	@iOSXCUITFindBy(id="Pay")
	@AndroidFindBy(id = "tv_pay")
	public MobileElement save_pay_header;
	
	@iOSXCUITFindBy(id="Done!")
	@AndroidFindBy(id = "tv_done")
	public MobileElement save_done_header;

	@iOSXCUITFindBy(id="BEST PRICE GUARANTEED")
	@AndroidFindBy(id = "tv_best_price_guaranteed")
	public MobileElement bestpriceheadertext;

	@iOSXCUITFindBy(id="tv_card_img_title")
	@AndroidFindBy(id = "tv_room_title")
	public MobileElement room_title;

	@iOSXCUITFindBy(id="tv_pay_detail_incl_price")
	@AndroidFindBy(id = "tv_price_per_night_value")
	public MobileElement pricepernight;

	@iOSXCUITFindBy(id="tv_right_lbl_info")
	@AndroidFindBy(id = "tv_rate_type_value")
	public MobileElement bookingtype;
	
	@iOSXCUITFindBy(id="tv_pay_detail_incl_right")
	@AndroidFindBy(id = "tv_breakfast_value")
	public MobileElement breakfast_value;

	@iOSXCUITFindBy(id="tv_pay_detail_incl_right")
	@AndroidFindBy(id = "tv_guest_value")
	public MobileElement no_guest_value;

	@iOSXCUITFindBy(id="tv_contact_info_lbl")
	@AndroidFindBy(id = "ll_contact_info_add_container")
	public MobileElement contact_info_button;

	@iOSXCUITFindBy(id="2_textField")
	@AndroidFindBy(id = "et_first_name")
	public MobileElement first_name;

	@iOSXCUITFindBy(id="3_textField")
	@AndroidFindBy(id = "et_last_name")
	public MobileElement last_name;

	@iOSXCUITFindBy(id="4_textField")
	@AndroidFindBy(id = "et_email")
	public MobileElement email;

	@iOSXCUITFindBy(id="5_phoneTextField")
	@AndroidFindBy(id = "et_phone_number")
	public MobileElement phone;

	@iOSXCUITFindBy(id="tv_chk_img")
	@AndroidFindBy(id = "cb_sms_confirmation")
	public MobileElement smsconfirmation_checkbox;

	@iOSXCUITFindBy(id="Save_button")
	@AndroidFindBy(id = "button_save")
	public MobileElement save_button;
	
	@AndroidFindBy(id="cb_late_arrival")
	@iOSXCUITFindBy(xpath="(//XCUIElementTypeImage[@name=\"tv_chk_img\"])[2]")
	public MobileElement guarantee_entry_checkbox;
	
	
	@AndroidFindBy(id="tv_cb_late_arrival_title")
	@iOSXCUITFindBy(id="tv_hotel_title_cell")
	public MobileElement guarantee_entry_checked_text;

	@iOSXCUITFindBy(id="tv_hotel_stay_name")
	@AndroidFindBy(id = "tv_hotel_name")
	public MobileElement hotel_name;

	@iOSXCUITFindBy(id="tv_hotel_stay_addr")
	@AndroidFindBy(id = "tv_hotel_address")
	public MobileElement hotel_add;
	
	@iOSXCUITFindBy(id="tv_hotel_stay_nights")
	@AndroidFindBy(id = "tv_number_of_nights_value")
	public MobileElement number_of_nights;
	
	@iOSXCUITFindBy(id="tv_pay_detail_incl_right")
	@AndroidFindBy(id = "tv_rate_type_price")
	public MobileElement price_details;
	
	@iOSXCUITFindBy(id="tv_pay_detail_incl_left")
	@AndroidFindBy(id = "tv_rate_type_name")
	public MobileElement pricedetails_bookingtype;
	
	@iOSXCUITFindBy(id="tv_checkbox_img")
	@AndroidFindBy(id = "cb_accept_terms")
	public MobileElement terms_condition_checkbox;

	@iOSXCUITFindBy(id="tv_pay_price_title_right")
	@AndroidFindBy(id = "tv_price")
	public MobileElement total_final_price;
	
	@iOSXCUITFindBy(id="book_button")
	@AndroidFindBy(id = "b_bottom_bar_pay")
	public MobileElement book_button;

	@iOSXCUITFindBy(id="tv_bottom_bar_total_price_value")
	@AndroidFindBy(id = "tv_bottom_bar_total_price")
	public MobileElement bottom_price_final;
	
	@iOSXCUITFindBy(id="b_bottom_bar_pay")
	@AndroidFindBy(id = "b_bottom_bar_pay")
	public MobileElement paywithcard_button;
	
	@iOSXCUITFindBy(id="tv_apple_pay_button")
	public MobileElement payApplePay_button;

	@AndroidFindBy(id = "tv_price_partial")
	public MobileElement voucher_remaing_price;

	public String getflexconfirmheadertitle() {
		return flex_confirm_header.getText();
	}
	
	public String getsaveconfirmheadertitle() {
		return save_confirm_header.getText();
	}
	
	public String getsavepayheadertitle() {
		return save_pay_header.getText();
	}
	
	public String getsavedoneheadertitle() {
		return save_done_header.getText();
	}

	public String getbestpriceheadertext() {
		return bestpriceheadertext.getText();
	}
	
	public String getRoomtitle() {
		return room_title.getText();
	}

	public String getPriceperNightdetails() {
		return pricepernight.getText();
	}

	public String getBookingType() {
		return bookingtype.getText();
	}
	
	public String getBreakfastType() {
		return breakfast_value.getText();
	}

	public String getguestNumber() {
		return no_guest_value.getText();
	}

	public String getHotelname() {
		return hotel_name.getText();
	}
	
	public String getnumberofNights() {
		return number_of_nights.getText();
	}

	public String getHoteladdress() {
		return hotel_add.getText();
	}

	public String getPricedetails() {
		return price_details.getText();
	}

	public String getVoucherPricedetails() {
		return price_details.getText()+" "+ voucher_remaing_price.getText();
	}

	public String gettotalfinalPrice() {
		return total_final_price.getText();
	}

	public String gettotalVoucherfinalPrice() {
		return total_final_price.getText() +" "+ voucher_remaing_price.getText();
	}

	public String getbottomFinalprice() {
		return bottom_price_final.getText();
	}

	public void clickoncontactInfobox() {
		contact_info_button.click();
	}

	public void enterBookingInformation(String first, String last,String emailn, String phonen) {
		if(getDriver().getCapabilities().getCapability("platformName").toString().equalsIgnoreCase("android")) {
		first_name.sendKeys(first);
		last_name.sendKeys(last);
		email.sendKeys(emailn);
		phone.sendKeys(phonen);
		}
		else {
			first_name.click();
			first_name.sendKeys(first);
			last_name.sendKeys(last);
			email.sendKeys(emailn);
			phone.sendKeys(phonen);
			getDriver().findElement(By.id("Done")).click();
		}

	}
	
	
	public void clickonsmsConfirmationCheckbox() {
		smsconfirmation_checkbox.click();
	}
	
	public void clickonstermsconditionConfirmationCheckbox() {
		terms_condition_checkbox.click();
	}

	public void savebutton() {
		save_button.click();
	}

	public void clickonBookbutton() {
		book_button.click();
	}
	
	public void clickonPaywithcardbutton() {
		paywithcard_button.click();
	}
	
	public void clickonGuaranteeentrycheckbox() {
		guarantee_entry_checkbox.click();
	}
	
	public String getcheckGuaranteeentrycheckboxischeckedtext() {
		return guarantee_entry_checked_text.getText();
	}
	
	public int Total_Number_Nights() {

		String[] night_text =getnumberofNights().split(" ");

		String night_number = night_text[0];
		int Total_Number_Nights = Integer.parseInt(night_number);

		return Total_Number_Nights;
	}
}
