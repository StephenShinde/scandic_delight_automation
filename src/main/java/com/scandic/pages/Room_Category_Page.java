package com.scandic.pages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Room_Category_Page {
	
	public Room_Category_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@iOSXCUITFindBy(id="Back")
	@AndroidFindBy(className = "android.widget.ImageButton")
	public MobileElement back_button;
	
	@iOSXCUITFindBy(className="XCUIElementTypeNavigationBar")
	@AndroidFindBy(id = "toolbar")
	public MobileElement hotel_title;
	
	@iOSXCUITFindBy(id="tv_hotel_address")
	@AndroidFindBy(id = "tv_hotel_address")
	public MobileElement hotel_address;
	
	@iOSXCUITFindBy(id="tv_hotel_distance")
	@AndroidFindBy(id = "tv_hotel_distance")
	public MobileElement distance;
	
	@iOSXCUITFindBy(id="tv_about_hotel")
	@AndroidFindBy(id = "tv_about_hotel")
	public MobileElement about_hotel;
	
	@iOSXCUITFindBy(id="tv_room_name")
	@AndroidFindBy(id = "tv_room_name")
	public List<MobileElement> room_name;
	
	@iOSXCUITFindBy(id="tv_square_metre")
	@AndroidFindBy(id = "tv_square_metre")
	public List <MobileElement>room_size;
	
	@iOSXCUITFindBy(id="tv_number_of_persons")
	@AndroidFindBy(id = "tv_number_of_persons")
	public List <MobileElement> room_capacityguest_number;
	
	@iOSXCUITFindBy(id="tv_primary_rate")
	@AndroidFindBy(id = "tv_primary_rate")
	public List <MobileElement> room_price;
	
	public String getHotelName() {
		return hotel_title.getText();
	}
	
	public String getAddress() {
		return hotel_address.getText();
	}
	
	public String getHoteldistance() {
		return distance.getText();
	}
	
	public String getRoomtypeName() {
		return room_name.get(0).getText();
	}
	
	public String getroomArea() {
		return room_size.get(0).getText();
	}
	
	public String getroomcapacityGuest() {
		return room_capacityguest_number.get(0).getText();
	}
	
	public String getRoomprice() {
		return room_price.get(0).getText();
	}
	
	public void clickonRoomtype() {
		room_name.get(0).click();
	}

}
