package com.scandic.pages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Search_Hotel_Result_Page {

	public Search_Hotel_Result_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@iOSXCUITFindBy(id="Book Hotel_tab")
	@AndroidFindBy(id = "action_book")
	public MobileElement Book_Hotel_Menu;
	
	@iOSXCUITFindBy(id="search_textField")
	@AndroidFindBy(id = "et_search_input")
	public MobileElement searchTextField;

	@iOSXCUITFindBy(id="calendar_button")
	@AndroidFindBy(id = "tv_top_bar_dates")
	public MobileElement calendarbutton;

	@iOSXCUITFindBy(id="guests_button")
	@AndroidFindBy(id = "tv_top_bar_guests")
	public MobileElement addguest_button;

	@iOSXCUITFindBy(id="guests_button")
	@AndroidFindBy(id = "tv_title")
	public MobileElement guestroom_moadlheader;

	@iOSXCUITFindBy(id="guests_button")
	@AndroidFindBy(id = "iv_cta_adult_add")
	public MobileElement add_adult_button;

	@iOSXCUITFindBy(id="guests_button")
	@AndroidFindBy(id = "iv_cta_adult_remove")
	public MobileElement remove_adult_button;

	@iOSXCUITFindBy(id="guests_button")
	@AndroidFindBy(id = "tv_select_adults")
	public MobileElement adult_count;

	@iOSXCUITFindBy(id="guests_button")
	@AndroidFindBy(id = "iv_cta_child_add")
	public MobileElement add_child_button;

	@iOSXCUITFindBy(id="guests_button")
	@AndroidFindBy(id = "iv_cta_child_remove")
	public MobileElement remove_child_button;

	@iOSXCUITFindBy(id="guests_button")
	@AndroidFindBy(id = "tv_select_children")
	public MobileElement child_count;

	@iOSXCUITFindBy(id="map_button")
	@AndroidFindBy(id = "tv_map")
	public MobileElement map_button;

	@iOSXCUITFindBy(id="searchOptions_button")
	@AndroidFindBy(id = "fl_booking_options")
	public MobileElement code_rewar_button;

	@iOSXCUITFindBy(id="searchOptions_button")
	@AndroidFindBy(id = "tv_title")
	public MobileElement rewardNightmodal;


	@iOSXCUITFindBy(id="searchOptions_button")
	@AndroidFindBy(id = "radio_button")
	public List<MobileElement> rewardNightmodal_radiobutton;


	@iOSXCUITFindBy(id="searchOptions_button")
	@AndroidFindBy(id = "tv_rate_code")
	public MobileElement promotional_code_textbox;
	
	@iOSXCUITFindBy(id="sort_textField")
	@AndroidFindBy(id = "tv_filter_text")
	public MobileElement filter_button;
	
	@iOSXCUITFindBy(className="XCUIElementTypeCell")
	@AndroidFindBy(id = "action_book")
	public MobileElement hotel_list_cellgroup;

	@iOSXCUITFindBy(id="tv_hotel_name")
	@AndroidFindBy(id = "tv_hotel_name")
	public MobileElement hotel_list_name;
	
	@iOSXCUITFindBy(id="tv_price")
	@AndroidFindBy(id = "tv_price")
	public MobileElement hotel_list_price;

	@iOSXCUITFindBy(id="tv_search_distance_km")
	@AndroidFindBy(id = "tv_search_distance_km")
	public List<MobileElement> hotel_list_distance;
	
	public String getsearchresultHotelName() {
		return hotel_list_name.getText();
	}
	
	public String getsearchresultHotelPrice() {
		return hotel_list_price.getText();
	}
	
	public void clickonSearchresulthotel() {
		hotel_list_name.click();
	}

	public void clickonguestbutton(){
		addguest_button.click();
	}
	public String getroomsguestsModaltext(){
		return guestroom_moadlheader.getText();
	}

	public void clickadultbutton(){
		add_adult_button.click();
	}

	public void clickremoveadultbutton(){
		remove_adult_button.click();
	}

	public String getadultcount(){
		return adult_count.getText();
	}

	public void clickchildbutton(){
		add_child_button.click();
	}

	public void clickremovechildbutton(){
		remove_child_button.click();
	}
	public String getchildcount(){
		return child_count.getText();
	}




	public void clickoncodesrewardsbutton(){
		code_rewar_button.click();
	}

	public String getrewardnightmodaltext(){
		return rewardNightmodal.getText();
	}

	public void clickonpromotioncoderadiobutton(){
		rewardNightmodal_radiobutton.get(0).click();
	}

	public void enterPromotionalcode(String promotionCode){
		promotional_code_textbox.sendKeys(promotionCode);
	}

	public void clickonbonuschequeradiobutton(){
		rewardNightmodal_radiobutton.get(2).click();
	}

}
