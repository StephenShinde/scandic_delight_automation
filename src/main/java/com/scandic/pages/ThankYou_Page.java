package com.scandic.pages;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class ThankYou_Page {
	
		
		public ThankYou_Page(AppiumDriver<MobileElement> driver) {
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		}
		
		@iOSXCUITFindBy(id="THANK YOU")
		@AndroidFindBy(id = "tv_thank_you")
		public MobileElement thankyou_text;
		
		@iOSXCUITFindBy(id="YOUR BOOKING CONFIRMATION HAS BEEN SENT TO YOUR EMAIL")
		@AndroidFindBy(id = "tv_added_to_my_stay")
		public MobileElement bookingconfirmation_text;
		
		@iOSXCUITFindBy(id="This hotel is cash free.")
		@AndroidFindBy(id = "tv_hotel_is_cash_free")
		public MobileElement cashfreenote_text;

		@iOSXCUITFindBy(id="Add to Calendar")
		@AndroidFindBy(id = "tv_calendar_thanks")
		public MobileElement addtocalendar_button;
		
		@iOSXCUITFindBy(id="Done")
		@AndroidFindBy(id = "b_done")
		public MobileElement Done_button;
		
		public String getthankyouText() {
			return thankyou_text.getText();
		}
		
		public String getbookingconfirmationtext() {
			return bookingconfirmation_text.getText();
		}
		
		public String getcashfreenotetext() {
			return cashfreenote_text.getText();
		}
		
		public void clickonDonebutton() {
			Done_button.click();
		}
		




}
