package com.scandic.pages;

import org.openqa.selenium.support.PageFactory;

import com.scandic.base.CreateDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Launch_Page {

	public Launch_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@iOSXCUITFindBy(id="login_button")
	@AndroidFindBy(id = "tv_sign_in")
	public MobileElement Login_button;

	@iOSXCUITFindBy(id="signup_button")
	@AndroidFindBy(id = "tv_sign_up")
	public MobileElement Signup_button;

	@iOSXCUITFindBy(id="later_button")
	@AndroidFindBy(id = "tv_text_later")
	public MobileElement Later_link;

	public String getLogintext() {
		return Login_button.getText();
	}
	 
	public String getSignuptext() {
		return Signup_button.getText();
	}
	
	public String getLatertext() {
		return Later_link.getText();
	}
	
	
	public void clickonLoginbutton() {
		Login_button.click();
	}

	public void clickonSignupbutton() {
		Signup_button.click();
	}

	public void clickonLaterLink() {
		Later_link.click();
	}

}


