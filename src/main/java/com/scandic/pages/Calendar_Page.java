package com.scandic.pages;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Calendar_Page {
	
	public Calendar_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name='Cancel']") 
	@AndroidFindBy(id = "iv_cancel")
	public MobileElement Calendar_Cancel_button;
	
	@iOSXCUITFindBy(id="Search dates_label")
	@AndroidFindBy(id = "tv_title")
	public MobileElement Calendar_Header_Text;

	@iOSXCUITFindBy(id="Show Results_button")
	@AndroidFindBy(id = "button_save")
	public MobileElement Calendar_Showresult;
	
	
    public String getCalenderheadertext() {
    	return Calendar_Header_Text.getText();
    }
	
	public void clickOncheckIndate() {
		
	}
	
	public void clickOncheckOutdate() {
		
	}
	
	public void clickonShowResults() {
		Calendar_Showresult.click();
	}

}
