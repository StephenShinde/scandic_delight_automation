package com.scandic.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.scandic.base.CreateDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Nets_Payment_Page extends CreateDriver{
	
	public Nets_Payment_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@iOSXCUITFindBy(id="Scandic")
	@AndroidFindBy(xpath = "//android.widget.Image[@content-desc=\"Scandic\"]")
	public MobileElement netspayment_header;
	

	@iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name='Done']")
	@AndroidFindBy(id = "close_button")
	public MobileElement done_button;

	@iOSXCUITFindBy(xpath="//XCUIElementTypeOther[@name=\\\"NETS Netaxept\\\"]/XCUIElementTypeOther[13]/XCUIElementTypeTextField")
	@AndroidFindBy(xpath = "//android.webkit.WebView[@content-desc=\"Accept payment - Nets Netaxept\"]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText")
	public MobileElement cardetails_textfield;
	
	@iOSXCUITFindBy(xpath="//XCUIElementTypeOther[@name=\\\"NETS Netaxept\\\"]/XCUIElementTypeOther[13]/XCUIElementTypeTextField")
	@AndroidFindBy(className = "android.widget.EditText")
	public MobileElement cvv_textfield;
	
	
	

	@AndroidFindBy(className = "android.widget.Button")
	public List<MobileElement> pay_button;
	
	@iOSXCUITFindBy(id="Cancel")
	@AndroidFindBy(xpath = "//android.widget.Button[@content-desc=\"Cancel\"]")
	public MobileElement cancel_button;
	

	public void enterCVV(String CVV){
		cvv_textfield.sendKeys(CVV);
	}

	public void clickonPaybutton(){
		pay_button.get(1).click();
	}
	

}
