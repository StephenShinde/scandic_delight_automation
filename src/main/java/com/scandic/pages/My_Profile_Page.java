package com.scandic.pages;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class My_Profile_Page {
	
	public My_Profile_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@iOSXCUITFindBy(id="My Profile_tab")
	@AndroidFindBy(id = "action_profile")
	public MobileElement My_Profile_menu;
	
	@iOSXCUITFindBy(id="login_button")
	@AndroidFindBy(id = "tv_top_bar_title")
	public MobileElement My_Profile_title;
	
	//@AndroidBy(uiAutomator="")
	@iOSXCUITFindBy(id="Log Out")
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"Log Out\")")
	public MobileElement Logout;
	
	@iOSXCUITFindBy(id="Log Out")
	@AndroidFindBy(id =  "android:id/button1")
	public MobileElement popUp_logout;
	

	
	
		public void clickonmyProfilemenu() {
			My_Profile_menu.click();
		}

		public void clickOnLogout(){
			Logout.click();
		}
}
