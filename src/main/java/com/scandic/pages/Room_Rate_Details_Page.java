package com.scandic.pages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import com.scandic.base.CreateDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Room_Rate_Details_Page extends CreateDriver {

	public Room_Rate_Details_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@iOSXCUITFindBy(id="multi_room_back_btn")
	@AndroidFindBy(className = "android.widget.ImageButton")
	public MobileElement back_button;

	@iOSXCUITFindBy(className="XCUIElementTypeNavigationBar")
	@AndroidFindBy(className = "android.widget.TextView")
	public MobileElement hotel_title;

	@iOSXCUITFindBy(id="tv_room_name")
	@AndroidFindBy(id = "tv_room_name")
	public MobileElement room_name;

	@iOSXCUITFindBy(id="tv_square_metre")
	@AndroidFindBy(id = "tv_square_metre")
	public MobileElement room_area;

	@iOSXCUITFindBy(id="tv_number_of_persons")
	@AndroidFindBy(id = "tv_number_of_persons")
	public MobileElement guest_capacity;

	@iOSXCUITFindBy(id = "b_book")
	@AndroidFindBy(id = "b_book")
	public List<MobileElement> book_buttons;

	@iOSXCUITFindBy(id = "b_book")
	@AndroidFindBy(id = "b_book")
	public MobileElement code_book_buttons;


	@iOSXCUITFindBy(id = "b_book")
	@AndroidFindBy(id = "tv_booking_code")
	public MobileElement booking_code;

	@AndroidFindBy(id = "tv_price")
	public List<MobileElement> android_hotel_price;

	@AndroidFindBy(id = "tv_price_value")
	public List<MobileElement> android_hotel_price_value;

	@iOSXCUITFindBy(id = "tv_price1")
	public List<MobileElement> ios_hotel_price_currency;

	@iOSXCUITFindBy(id="tv_rate_type")
	@AndroidFindBy(id="tv_title")
	public List<MobileElement> bookingtext;


	@AndroidFindBy(id="tv_title")
	public MobileElement Gift_Voucher_bookingtext;

	@iOSXCUITFindBy(id="lnk_room_facilities")
	@AndroidFindBy(id = "iv_next")
	public MobileElement About_room_link;

	@iOSXCUITFindBy(id="About the room")
	@AndroidFindBy(id = "tv_title_facilities")
	public MobileElement About_room_headertext;

	@AndroidFindBy(id = "tv_promo_code_ext")
	public MobileElement ScandicFriends_member_text;

	@AndroidFindBy(id = "tv_price")
	public MobileElement voucher_price;


	@AndroidFindBy(id = "tv_partial_price")
	public MobileElement BC_currency_price;


	@AndroidFindBy(id = "tv_partial_price_value")
	public MobileElement BC_currency_value;

	public String gethotelTitle() {
		return hotel_title.getText();
	}

	public String getRoomtypeName() {
		return room_name.getText();
	}

	public String getRoomarea() {
		return room_area.getText();
	}

	public String getguestcapacity() {
		return guest_capacity.getText();
	}

	public String getandroidSavebookingPrice() {
		return android_hotel_price.get(0).getText();
	}

	public String getandroidSavebookingcurrency() {

		return android_hotel_price_value.get(0).getText();
	}

	public String getandroidFlexbookingPrice() {
		return android_hotel_price.get(2).getText();
	}

	public String getandroidFlexbookingcurrency() {

		return android_hotel_price_value.get(1).getText();
	}

	public String getandroidscandicfriendsbookingbookingPrice() {
		return android_hotel_price.get(0).getText();
	}

	public String getandroidscandicfriendsbookingbookingcurrency() {

		return android_hotel_price_value.get(0).getText();
	}


	public String getSavebookingprice() {
		if(getDriver().getCapabilities().getCapability("platformName").toString().equalsIgnoreCase("android")) {

			return getandroidSavebookingPrice()+getandroidSavebookingcurrency();
		}
		else {
			return ios_hotel_price_currency.get(0).getText();
		}
	}


	public String getFlexbookingPrice() {
		if(getDriver().getCapabilities().getCapability("platformName").toString().equalsIgnoreCase("android")) {

			return getandroidFlexbookingPrice()+getandroidFlexbookingcurrency();
		}
		else {
			return ios_hotel_price_currency.get(1).getText();
		}
	}


	public void clickonSavebooking() {
		book_buttons.get(0).click();
	}

	public void clickonFlexbooking() {
		book_buttons.get(2).click();
	}

	public String getSavebookingtext() {
		return bookingtext.get(0).getText();
	}

	public String getFlexbookingtext() {
		return bookingtext.get(2).getText();
	}

	public String getScandicfriendsookingtext() {
		return bookingtext.get(0).getText();
	}

	public String getBookingcode(){
		return booking_code.getText();
	}

	public String getGiftVouchertext(){
		return Gift_Voucher_bookingtext.getText();
	}

	public void clickonDiscountScandiccodebookbutton(){
		book_buttons.get(0).click();
	}


	public String getdiscountscandicfriendsbookingPrice() {
		if(getDriver().getCapabilities().getCapability("platformName").toString().equalsIgnoreCase("android")) {

			return getandroidscandicfriendsbookingbookingPrice()+getandroidscandicfriendsbookingbookingcurrency();
		}
		else {
			return ios_hotel_price_currency.get(2).getText();
		}
	}

	public String getScandicFriendMembertext(){
		return ScandicFriends_member_text.getText();
	}

	public String getGift_Voucherprice(){
		return voucher_price.getText();
	}

	public void clickonGiftVoucherBookbutton() {
		book_buttons.get(0).click();
	}

	public String getBonuschequetext(){
		return Gift_Voucher_bookingtext.getText();
	}

	public String getBonus_chequeprice(){
		return voucher_price.getText() + " "+BC_currency_price.getText()+BC_currency_value.getText();
	}
}
