package com.scandic.pages;

import org.openqa.selenium.support.PageFactory;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Signup_Page{
	
	public Signup_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name='Cancel']")
	@AndroidFindBy(id = "iv_cancel")
	public MobileElement Cancel_button;
	
	@iOSXCUITFindBy(id="Join Scandic Friends_label")
	@AndroidFindBy(id = "tv_title")
	public MobileElement Signup_Page_Title;
	
	@iOSXCUITFindBy(id="1_textField")
	@AndroidFindBy(id = "et_first_name")
	public MobileElement First_Name;

	@iOSXCUITFindBy(id="2_textField")
	@AndroidFindBy(id = "et_last_name")
	public MobileElement Last_Name;
	
	
	@iOSXCUITFindBy(id="3_textField")
	@AndroidFindBy(id = "et_email")
	public MobileElement Email;
	
	@iOSXCUITFindBy(id="4_textField")
	@AndroidFindBy(id = "et_password")
	public MobileElement New_Pass;
	
	@iOSXCUITFindBy(id="5_textField")
	@AndroidFindBy(id = "et_confirm_password")
	public MobileElement Confirm_Pass;
	
	@iOSXCUITFindBy(id="6_countryTextField")
	@AndroidFindBy(id = "fl_phone_number_area_code_container")
	public MobileElement Country_Code;
	
	@iOSXCUITFindBy(id="6_phoneTextField")
	@AndroidFindBy(id = "et_phone_number")
	public MobileElement Mobile_Num;

	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"SELECT\")")
	public MobileElement select_button;
	
	@iOSXCUITFindBy(id="Done")
	public MobileElement keypad_done;
	
	
	
	@iOSXCUITFindBy(id="7_textField")
	@AndroidFindBy(id = "tv_select_birthday")
	public MobileElement Date_Birth;
	
	@iOSXCUITFindBy(id="8_textField")
	@AndroidFindBy(id = "tv_select_country_input")
	public MobileElement Country;
	
	@AndroidFindBy(id = "cb_accept_terms")
	public MobileElement Terms_Condition_Checkbox;
	
	@iOSXCUITFindBy(id="I accept Terms & Conditions")
	@AndroidFindBy(id = "tv_read_more")
	public MobileElement Terms_Condition_text;
	
	
	@iOSXCUITFindBy(id="Join Now_button")
	@AndroidFindBy(id = "button_save")
	public MobileElement SignUp_button;

	@AndroidFindBy(id = "tv_header")
	public MobileElement SignUp_thank;

	@AndroidFindBy(id = "btn_next")
	public MobileElement SignUp_thank_next;

	@iOSXCUITFindBy(id="later_button")
	@AndroidFindBy(id = "btn_later")
	public MobileElement Later_link;

	
	public String getSignupTitle() {
		return Signup_Page_Title.getText();
	}
	
	public void enterFirstName(String firstName) {
		First_Name.click();
		First_Name.sendKeys(firstName);
		
		}

	
	public void enterLastName(String lastName) {
		Last_Name.click();
		Last_Name.sendKeys(lastName);
		}
	
	public void enterEmail(String email) {
		Email.click();
		Email.sendKeys(email);
	}
	
	public void enterNewPassword(String newPassword) {
		New_Pass.click();
		New_Pass.sendKeys(newPassword);

	}
	
	public void enterConfirmPassword(String confirmPassword) {
		Confirm_Pass.click();
		Confirm_Pass.sendKeys(confirmPassword);
	}
	
	public void enterMobileNumber(String mobileNumber) {

		Mobile_Num.click();
		Mobile_Num.sendKeys(mobileNumber);

	}

	public void click_Country_code(){
		Country_Code.click();
		select_button.click();
	}
	public void select_dob(){
		Date_Birth.click();
		select_button.click();
	}

	public void select_country(){
		Country.click();
		select_button.click();
	}

	public void click_TC(){
		Terms_Condition_Checkbox.click();
	}

	public void click_signup(){
		SignUp_button.click();
	}

	public String get_Thanks_text(){
		return SignUp_thank.getText();
	}


	public void click_next(){
		SignUp_thank_next.click();
	}

	public void click_later(){
		Later_link.click();
	}
	
	

}
