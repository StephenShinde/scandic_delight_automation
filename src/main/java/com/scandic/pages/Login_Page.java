package com.scandic.pages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import com.scandic.base.CreateDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Login_Page extends CreateDriver {

	public Login_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"“Scandic Stage” Wants to Use “scandichotels.no” to Sign In\"]")
	@AndroidFindBy(id = "action_book")
	public MobileElement Login_navigation_popup;

	@iOSXCUITFindBy(id="Continue")
	@AndroidFindBy(id = "tv_book_hotel_text")
	public MobileElement Continue_button;

	
	@iOSXCUITFindBy(id="Cancel")
	@AndroidFindBy(id = "Close tab")
	public MobileElement Cancel_button;

	@iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"LOG IN\"]")
	@AndroidFindBy(xpath = "//android.view.View[@content-desc=\"LOG IN\"]")
	public MobileElement Loggin_banner;


	@iOSXCUITFindBy(className="XCUIElementTypeTextField")
	@AndroidFindBy(className = "android.widget.EditText")
	public List <MobileElement> Email;

	@iOSXCUITFindBy(className="XCUIElementTypeSecureTextField")
	public MobileElement ios_Password;
	
	@AndroidFindBy(className = "android.widget.EditText")
	public List <MobileElement> android_Password;

	@iOSXCUITFindBy(className="XCUIElementTypeButton")
	@AndroidFindBy(className = "android.widget.Button")
	public MobileElement LogIn;

	@iOSXCUITFindBy(className="XCUIElementTypeLink")
	@AndroidFindBy(id = "tv_book_hotel_text")
	public MobileElement Forgot_password;

	public String getLoginbannertext() {

		return Loggin_banner.getText();

	}


	public void clickonContinuePopup() {
		Continue_button.click();
	}

	public void clickonCancelpopup() {
		Cancel_button.click();
	}


	public void click_email() {
		
			Email.get(0).click();
	}

	public void clearEmailfield() {
		Email.get(0).clear();
	}


	public void enter_email(String email) {
		Email.get(0).sendKeys(email);
	}

	public void click_android_password() {
		
			android_Password.get(1).click();
		}
	
	public void click_ios_password() {
		
		ios_Password.click();
	}
	
	

	public void enter_android_password(String pass) {
		android_Password.get(1).sendKeys(pass);
		getDriver().hideKeyboard();
		}
		
	public void enter_ios_password(String pass) {
		ios_Password.sendKeys(pass);
		getDriver().hideKeyboard();
		}
	

	public void click_Login() {
		LogIn.click();
	}
}
