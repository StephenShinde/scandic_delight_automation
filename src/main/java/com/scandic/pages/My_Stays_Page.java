package com.scandic.pages;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class My_Stays_Page {
	
	public My_Stays_Page(AppiumDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@iOSXCUITFindBy(id="Book Hotel_tab")
	@AndroidFindBy(id = "action_my_stays")
	public MobileElement My_Stays_Menu;
	
	@iOSXCUITFindBy(id="Book Hotel_tab")
	@AndroidFindBy(id = "tv_top_bar_title")
	public MobileElement My_Stays_page_lable;
	
//	@iOSXCUITFindBy(id="Book Hotel_tab")
//	@AndroidFindBy(id = "tv_top_bar_title")
//	public MobileElement My_Stays_page_lable;

	
}
