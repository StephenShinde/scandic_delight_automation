package com.scandic.Testutilities;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class TestUtil {

	public String searchbycityname() {
		List<String> givenList = Arrays.asList("Stockholm","Oslo", "Gothenburg", "Malmo", "Berlin","Alesund", "Copenhagen" ,"Helsinki");
		Random rand = new Random();
		String randomElement = givenList.get(rand.nextInt(givenList.size()));

		return randomElement;
	}


	public String searchbyHotelname() {
		List<String> givenList = Arrays.asList("Scandic Continental","Scandic Grand Central", "Scandic Klara",
											"Scandic Anglais","Scandic No. 53","Scandic Park","Scandic Crown","Grand Hotel Oslo by Scandic",
											"Scandic Byporten","Scandic Palace Hotel", "Scandic Webers");
		Random rand = new Random();
		String randomElement = givenList.get(rand.nextInt(givenList.size()));

		return randomElement;
	}
	
	
	public String getMonth() {
		String[] monthName = {"January", "February",
				"March", "April", "May", "June", "July",
				"August", "September", "October", "November",
		"December"};

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH+6)];
		return month;

	}

	public String adddecimalinamount(int number){
		DecimalFormat df = new DecimalFormat("#,###");
		 String decimalnumber = df.format(number);
		 return decimalnumber;
	}
	
//	public void selectRandomHotelfromresult() {
//		List<MobileElement> elmenent= getDriver().findElementsByClassName("XCUIElementTypeCell");
//		System.out.println("Number of elements are     "+elmenent.size());
//		
//		Random rand = new Random();
//		MobileElement randomElement = elmenent.get(rand.nextInt(elmenent.size()));
//		randomElement.click();
//		
//		Thread.sleep(5000);
//		
//		for (int i=0; i<elmenent.size();i++){
//			
//		     elmenent.get(i).click();
//		    }
//					
////			Dimension sizeofelement=getDriver().findElement(By.className("XCUIElementTypeCell")).getSize();
////			System.out.println(sizeofelement);
//			
//			Thread.sleep(10000);
//		}
//	}

}
