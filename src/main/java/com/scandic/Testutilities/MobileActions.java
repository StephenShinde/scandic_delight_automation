package com.scandic.Testutilities;

import org.openqa.selenium.Dimension;

import com.scandic.ExternalUtilities.Log;
import com.scandic.base.CreateDriver;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;

import static io.appium.java_client.touch.offset.PointOption.point;
import org.openqa.selenium.Dimension;

import java.time.Duration;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;

public class MobileActions extends CreateDriver {

	public void scrollRightToLeft(MobileElement element, double startxpercentage, double endxpercentage){


		Dimension dim = getDriver().manage().window().getSize();
		int width = dim.getWidth();

		int x = element.getLocation().getX();
		//System.out.println(element.getLocation().getX());

		int y = element.getLocation().getY();
		//System.out.println(element.getLocation().getY());

		int start_x = (int) (width * startxpercentage);
		int end_x = (int) (width * endxpercentage);

		//System.out.println(start_x + "  " + end_x);
		System.out.println("Scroll Right to Left ");
		TouchAction ts = new TouchAction(getDriver());

		ts.press(point(end_x, y)).moveTo(point(start_x,y)).release().perform();
	}

	public void scrollLeftToRight(MobileElement element, double startxpercentage, double endxpercentage){


		Dimension dim = getDriver().manage().window().getSize();
		int width = dim.getWidth();

		int x = element.getLocation().getX();
		System.out.println(element.getLocation().getX());

		int y = element.getLocation().getY();
		System.out.println(element.getLocation().getY());

		int start_x = (int) (width * startxpercentage);
		int end_x = (int) (width * endxpercentage);

		System.out.println(start_x + "  " + end_x);
		System.out.println("Scroll Right to Left ");
		TouchAction ts = new TouchAction(getDriver());

		ts.press(point(start_x, y)).moveTo(point(end_x,y)).release().perform();
	}

	//  Swipe top to botton with full screenFingure moves upside Page Content goes down
	public void scrolltoBotton_fullscreen(double startypercentage, double endypercentage, double midxpercentage){

		Dimension dim = getDriver().manage().window().getSize();
		int height = dim.getHeight();
		int width = dim.getWidth();
		int mid_x = (int) (width * midxpercentage);
		int start_y = (int) (height * startypercentage);
		int end_y = (int) (height * endypercentage);

		TouchAction ts = new TouchAction(getDriver());
		ts.press(point(mid_x, start_y))
		.waitAction(waitOptions(ofMillis(1000)))
		.moveTo(point (mid_x, end_y))
		.release()
		.perform();
		System.out.println("scroll done");
	}



	// Swipe top to bottom with scrollable window, fingure moves upside page content goes down
	public void scrolltoBottom_Scrollablewindow(MobileElement scrollableWindowElement,double startypercentage, double endypercentage) {

		Dimension dim = scrollableWindowElement.getSize();
		int height = dim.getHeight();
		int width = dim.getWidth();
		int mid_x = width / 2;
		int start_y = (int) (height * startypercentage);
		int end_y = (int) (height * endypercentage);

		TouchAction ts = new TouchAction(getDriver());
		ts.press(point(mid_x, start_y))
		.waitAction(waitOptions(ofMillis(1000)))
		.moveTo(point (mid_x, end_y))
		.release()
		.perform();
		System.out.println("scroll done");
	}

	public void ScrollToexactElementandClick(MobileElement element, double startypercentage, double endypercentage, double midxpercentage) {
		
		while(true) {
			if (getIsDispalyedElement(element)) {
				element.click();
				break;
			} else {
				scrolltoBotton_fullscreen(0.7, 0.3, 0.5);
			}
		}
	}

	private boolean getIsDispalyedElement(MobileElement element) {
		boolean isDisplayed = false;
		try {
			isDisplayed = element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
		return isDisplayed;
	} 

public void ScrollToexactElement(MobileElement element, double startypercentage, double endypercentage, double midxpercentage) {
		
		while(true) {
			if (getIsDispalyedElement(element)) {
				Log.info("element is visible");
				break;
			} else {
				scrolltoBotton_fullscreen(0.7, 0.3, 0.5);
			}
		}
	}

}