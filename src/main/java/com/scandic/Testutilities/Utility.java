package com.scandic.Testutilities;

import java.io.IOException;

import org.apache.velocity.runtime.parser.node.GetExecutor;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.scandic.base.CreateDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;

public class Utility extends CreateDriver{
	public WebDriverWait wait;

	public void verifyTextPresent(String actual, String expected) {
		try {
			Assert.assertEquals(actual, expected);
		} catch (Exception e) {
			System.out.println("Text is not found");
			e.printStackTrace();
		}
	}


	public void verifyElementPresent(MobileElement element) {

		try {
			Assert.assertTrue(element.isDisplayed());
		} catch (Exception e) {
			System.out.println("element is not found");
			e.printStackTrace();
		}

	}

	public void waitTillElementsGetVisible(MobileElement locator, int timeout) throws IOException {

		try {

			wait=new WebDriverWait(getDriver(), timeout);
			MobileElement element = (MobileElement) wait.until(ExpectedConditions.visibilityOf(locator));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("element is not getting visible");
		}
	}

	public void wait_to_click(MobileElement locator, int timeout) {
		try {
			MobileElement element = (MobileElement) wait.until(ExpectedConditions.elementToBeClickable(locator));
			element.click();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Element is not clickable");
		}
	}

	public void clickWhenReady(MobileElement locator, int timeout) {

		WebElement element = null;

		WebDriverWait wait = new WebDriverWait(getDriver(), timeout);

		element = wait.until(ExpectedConditions.elementToBeClickable(locator));

		element.click();

	}
	public void hideKeyboard(){
		getDriver().hideKeyboard();
	}


}
