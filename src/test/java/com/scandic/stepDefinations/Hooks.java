package com.scandic.stepDefinations;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.scandic.ExternalUtilities.Log;
import com.scandic.base.CreateDriver;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks extends CreateDriver {

	@Before
	public void StartScenario(Scenario scenario) {
		Log.info(scenario.getName() + " scenario is starting");
	}

	@After
	public void embedScreenshot(Scenario scenario) throws Exception {
		Log.info(scenario.getName() + " scenario is ending");
		
		if (scenario.isFailed()) {
			try {
				byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
				String testName = scenario.getName();
				scenario.embed(screenshot, "image/png");
				scenario.write(testName);
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}



}


