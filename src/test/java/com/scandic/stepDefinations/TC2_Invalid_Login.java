package com.scandic.stepDefinations;

import com.scandic.Testutilities.MobileActions;
import com.scandic.Testutilities.Utility;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Launch_Page;
import com.scandic.pages.Login_Page;
import com.scandic.pages.My_Profile_Page;
import com.scandic.pages.Search_Hotel_Page;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TC2_Invalid_Login extends CreateDriver {
	
	Launch_Page launchpage=new Launch_Page(getDriver());
	Utility utility=new Utility();
	Login_Page loginpage=new Login_Page(getDriver());
	My_Profile_Page myprofilepage=new My_Profile_Page(getDriver());
	Search_Hotel_Page searchhotelpage=new Search_Hotel_Page(getDriver());
	MobileActions actions=new MobileActions();
	
	@When("^I enter invalid email and invalid password$") 
	public void I_enter_invalid_email_and_invalid_password() throws Throwable {
		//  LoginPage loginpage=new LoginPage(getDriver());
		    loginpage.click_email();
		    loginpage.clearEmailfield();
		    loginpage.enter_email("stephen");
		    if(getDriver().getCapabilities().getCapability("platformName").toString() == "iOS") {
				loginpage.click_ios_password();
				loginpage.enter_ios_password("Scandic123");
			}
			else {
				loginpage.click_android_password();
				loginpage.enter_android_password("scandic123");
			}
	}

	@Then("^I should get error message$") 
	public void I_should_get_error_message() throws Throwable {
	   System.out.println("Error");
	}


}
