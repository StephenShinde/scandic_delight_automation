package com.scandic.stepDefinations;

import org.openqa.selenium.By;

import com.scandic.ExternalUtilities.Log;
import com.scandic.Testutilities.MobileActions;
import com.scandic.Testutilities.Utility;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Confirm_Booking_Page;
import com.scandic.pages.Nets_Payment_Page;

import io.appium.java_client.MobileElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TC8_FlexBookingWithGuaranteeWithoutLogin extends CreateDriver {

	Confirm_Booking_Page confirmbookingpage=new Confirm_Booking_Page(getDriver());
	Nets_Payment_Page netspaymentpage=new Nets_Payment_Page(getDriver());
	Utility utility=new Utility();
	MobileActions mobilections=new MobileActions();
	
	@When("I click on guarantee your booking checkbox") 
	public void I_click_on_guarantee_your_booking_checkbox() throws Throwable {
		Log.info("Step Started: I click on guarantee your booking checkbox");
		
		mobilections.ScrollToexactElementandClick(confirmbookingpage.guarantee_entry_checkbox, 0.7, 0.3, 0.5);
		
		Log.info("Step Ended: I click on guarantee your booking checkbox");
	}


	@Then("I should navigate to netspayment screen") 
	public void I_should_navigate_to_netspayment_screen() throws Throwable {
		Log.info("Step Started: I should navigate to netspayment screen");
		
		utility.verifyElementPresent(netspaymentpage.netspayment_header);
		
		Log.info("Step Ended: I should navigate to netspayment screen");

	}

	@When("I enter card details")
	public void I_enter_card_details() throws Throwable {
		Log.info("Step Started: I enter card details");
		
		mobilections.ScrollToexactElement(netspaymentpage.cancel_button, 0.7, 0.3, 0.5);

		
		Log.info("Step Ended: I enter card details");
	}

	@When("I click on register button") 
	public void I_click_on_register_button() throws Throwable {
		Log.info("Step Started: I click on register button");
		

		
		Log.info("Step Ended: I click on register button");
	}



}
