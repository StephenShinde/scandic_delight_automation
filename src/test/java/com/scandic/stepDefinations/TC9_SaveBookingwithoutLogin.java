package com.scandic.stepDefinations;

import com.scandic.Testutilities.TestUtil;
import com.scandic.pages.Nets_Payment_Page;
import org.openqa.selenium.By;

import com.scandic.ExternalUtilities.Log;
import com.scandic.Testutilities.MobileActions;
import com.scandic.Testutilities.Utility;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Confirm_Booking_Page;
import com.scandic.pages.Room_Rate_Details_Page;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TC9_SaveBookingwithoutLogin extends CreateDriver{
	
	 Utility utility=new Utility();
	 Room_Rate_Details_Page roomratedetailspage=new Room_Rate_Details_Page(getDriver());
	 Confirm_Booking_Page confirmbookingpage=new Confirm_Booking_Page(getDriver());
	 Nets_Payment_Page netspaymentpage=new Nets_Payment_Page(getDriver());

	 String Savebookingtext;
	 String Save_Price;
	 String Save_price_currency;
	 String Save_price_currency_removenight;
	 String Save_proce_N;
	 int Save_Price_Value;
	String RoomName;

	@When("^I click on Save booking button$") 
	public void I_click_on_Save_booking_button() throws Throwable {
		Log.info("Step Started: I click on Save booking button");
		RoomName=roomratedetailspage.getRoomtypeName();
		Savebookingtext= roomratedetailspage.getSavebookingtext();
		Save_Price=roomratedetailspage.getSavebookingprice();

		// This logic is used when we select multiple nights stay
		String[] Save_Price_parts = Save_Price.split(" ");
		String Save_price_number = Save_Price_parts[0];
		Save_price_currency= Save_Price_parts[1];
		Save_price_currency_removenight= Save_price_currency.replace("/Night","");
		Save_proce_N=Save_price_number.replaceAll("[-+.^:,]","");

		Save_Price_Value= Integer.parseInt(Save_proce_N);
		
	    roomratedetailspage.clickonSavebooking();
	    
	    Log.info("Step Ended: I click on Save booking button");
	}

	@Then("^I should navigate to confirm Save booking screen$") 
	public void I_should_navigate_to_confirm_Save_booking_screen() throws Throwable {
		Log.info("Step Started: I should navigate to confirm Save booking screen");
	   
	    utility.verifyTextPresent(confirmbookingpage.getsaveconfirmheadertitle(), "Confirm");
	    utility.verifyTextPresent(confirmbookingpage.getsavepayheadertitle(), "Pay");
	    utility.verifyTextPresent(confirmbookingpage.getsavedoneheadertitle(), "Done!");
	    

	    // Verify Room Category Type
		utility.verifyTextPresent(confirmbookingpage.getRoomtitle(), RoomName);
		// Verify Price Per Night
		//Temporary Comment
		utility.verifyTextPresent(confirmbookingpage.getPriceperNightdetails(), Save_Price+"/Night");
		// Verify Flex booking Text
		utility.verifyTextPresent(confirmbookingpage.getBookingType(), Savebookingtext);

		Log.info("Step Ended: I should navigate to confirm Save booking screen");
	}

	@Then("^I click on Pay with card button to complete booking$") 
	public void I_click_on_Pay_with_card_button_to_complete_booking() throws Throwable {
		Log.info("Step Started: I click on Pay with card button to complete booking");
		// Logic to multiply number of nights with price
		int final_Value= Save_Price_Value * confirmbookingpage.Total_Number_Nights();
		TestUtil testUtil=new TestUtil();
		String Final_Value = testUtil.adddecimalinamount(final_Value);

		String Total_Flex_price = String.valueOf(final_Value);
		// Verify total price
		if (confirmbookingpage.getnumberofNights().equalsIgnoreCase("1 night")){
			utility.verifyTextPresent(confirmbookingpage.getPricedetails(), Save_Price);
			utility.verifyTextPresent(confirmbookingpage.gettotalfinalPrice(),Save_Price);
			utility.verifyTextPresent(confirmbookingpage.getbottomFinalprice(),Save_Price);

		}
		else{
			utility.verifyTextPresent(confirmbookingpage.getPricedetails(), Save_Price);
			utility.verifyTextPresent(confirmbookingpage.gettotalfinalPrice(), Final_Value+" "+Save_price_currency_removenight);

			utility.verifyTextPresent(confirmbookingpage.getbottomFinalprice(), Final_Value +" "+Save_price_currency_removenight);
		}
		
		confirmbookingpage.clickonPaywithcardbutton();
		
		Log.info("Step Ended: I click on Pay with card button to complete booking");
	}

	@When("^I click on Pay button$") 
	public void I_click_on_Pay_button() throws Throwable {
		Log.info("Step Started: I click on Pay button");
		utility.waitTillElementsGetVisible(netspaymentpage.cvv_textfield,240);
	    // Considering card already saved so just enter CVV number

		netspaymentpage.enterCVV("235");

		netspaymentpage.clickonPaybutton();

	    
	    Log.info("Step Ended: I click on Pay button");
	}

}
