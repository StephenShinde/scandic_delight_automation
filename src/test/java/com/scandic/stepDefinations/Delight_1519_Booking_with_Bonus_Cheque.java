package com.scandic.stepDefinations;

import com.scandic.ExternalUtilities.Log;
import com.scandic.Testutilities.Utility;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Confirm_Booking_Page;
import com.scandic.pages.Room_Rate_Details_Page;
import com.scandic.pages.Search_Hotel_Result_Page;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Delight_1519_Booking_with_Bonus_Cheque extends CreateDriver {

    Search_Hotel_Result_Page searchhotelresultpage=new Search_Hotel_Result_Page(getDriver());
    Room_Rate_Details_Page roomratedetailspage=new Room_Rate_Details_Page(getDriver());
    Confirm_Booking_Page confirmbookingpage=new Confirm_Booking_Page(getDriver());
    Utility utility=new Utility();
    String Bonus_cheque_Price;

    String Bonuscheque_Booking_text;


    @Then("I select bonus cheque checkbox")
    public void I_select_bonus_cheque_checkbox() throws Throwable {

        searchhotelresultpage.clickonbonuschequeradiobutton();


    }

    @When("I click on bonus cheque booking button")
    public void I_click_on_bonus_cheque_booking_button() throws Throwable {
        Bonuscheque_Booking_text=roomratedetailspage.getGiftVouchertext();

        Bonus_cheque_Price=roomratedetailspage.getBonus_chequeprice();
//        String[] GiftVoucherparts = Gift_Voucher_Price.split(" ");
//        String gift_voucher_price_number = GiftVoucherparts[0];
//        Gift_Voucher_Price_Value= Integer.parseInt(gift_voucher_price_number);
        roomratedetailspage.clickonGiftVoucherBookbutton();
        Thread.sleep(5000);
    }

    @Then("I should navigate to confirm bonus cheque booking screen")
    public void I_should_navigate_to_confirm_bonus_cheque_booking_screen() throws Throwable {
        Log.info("Step Started: I should navigate to confirm gift voucher booking screen");

        utility.verifyTextPresent(confirmbookingpage.getflexconfirmheadertitle(), "Confirm");
        utility.verifyTextPresent(confirmbookingpage.getbestpriceheadertext(), "BEST PRICE GUARANTEED");

        utility.verifyTextPresent(confirmbookingpage.getPriceperNightdetails(), Bonus_cheque_Price+"/Night");
        // Verify Flex booking Text
        utility.verifyTextPresent(confirmbookingpage.getBookingType() , Bonuscheque_Booking_text);

        Log.info("Step Ended: I should navigate to confirm gift voucher booking screen");

    }

    @Then("I click on bonus cheque book button to complete booking")
    public void I_click_on_bonus_cheque_book_button_to_complete_booking() throws Throwable {
        Log.info("Step Started: I click on voucher book button to complete booking");
        // Verify total price
        if (confirmbookingpage.getnumberofNights().equalsIgnoreCase("1 night")){
            System.out.println("1");
            utility.verifyTextPresent(confirmbookingpage.getVoucherPricedetails(), Bonus_cheque_Price);
            System.out.println("2");
            utility.verifyTextPresent(confirmbookingpage.gettotalVoucherfinalPrice(),Bonus_cheque_Price);
            System.out.println("3");
            //utility.verifyTextPresent(confirmbookingpage.getbottomFinalprice(), Bonus_cheque_Price);
        }
        else{
            //utility.verifyTextPresent(confirmbookingpage.getPricedetails(), Gift_Voucher_Price+"/Night");
        }

        //  Gift_Voucher_Price_Value
       // int final_Value= Gift_Voucher_Price_Value * confirmbookingpage.Total_Number_Nights();

      //  String Total_GiftVoucher_price = String.valueOf(final_Value);

       // utility.verifyTextPresent(confirmbookingpage.gettotalfinalPrice(), Total_GiftVoucher_price + " "+ "Voucher");

       // utility.verifyTextPresent(confirmbookingpage.getbottomFinalprice(), Total_GiftVoucher_price+ " "+ "Voucher");


        confirmbookingpage.clickonBookbutton();

        Log.info("Step Ended: I click on voucher book button to complete booking");
    }


}
