        package com.scandic.stepDefinations;

        import com.scandic.ExternalUtilities.Log;
        import com.scandic.Testutilities.TestUtil;
        import com.scandic.Testutilities.Utility;
        import com.scandic.base.CreateDriver;
        import com.scandic.pages.Confirm_Booking_Page;
        import com.scandic.pages.Room_Rate_Details_Page;
        import com.scandic.pages.Search_Hotel_Page;
        import com.scandic.pages.Search_Hotel_Result_Page;
        import io.cucumber.java.en.Then;
        import io.cucumber.java.en.When;

        public class Delight_656_Book_a_room_with_SF_discount_logged_in extends CreateDriver {

            Search_Hotel_Page searchHotelPage=new Search_Hotel_Page(getDriver());
            Search_Hotel_Result_Page searchHotelResultPage=new Search_Hotel_Result_Page(getDriver());
            Room_Rate_Details_Page roomRateDetailsPage=new Room_Rate_Details_Page(getDriver());
            Confirm_Booking_Page confirmBookingPage=new Confirm_Booking_Page(getDriver());
            Utility utility=new Utility();
            TestUtil testUtil=new TestUtil();

            String ScandicFriendsBooking_text;
            String ScandicFriendsBooking_Price;
            String SF_price_currency;
            String SF_price_currency_removenight;
            String SF_price_N;
            int SF_Price_Value;

            @Then("I enter City name {string} in search box")
            public void I_enter_City_name_in_search_box(String cityName) throws Throwable {
                Log.info("Step Started: I enter City name in search box");
                searchHotelPage.entercitynameSF(cityName);
                Log.info("Step Ended: I enter City name in search box");
            }

            @When("I click on codes and rewards nights")
            public void I_click_on_codes_and_rewards_nights() throws Throwable {
                Log.info("Step Started: I click on codes and rewards nights");
                searchHotelResultPage.clickoncodesrewardsbutton();
                Log.info("Step Ended: I click on codes and rewards nights");
            }

            @Then("I can see booking options modal")
            public void I_can_see_booking_options_modal() throws Throwable {
                Log.info("Step Started: I can see booking options modal");
                utility.verifyTextPresent(searchHotelResultPage.getrewardnightmodaltext(),"Booking Options");
                Log.info("Step Ended: I can see booking options modal");
            }

            @Then("I select promotional or corporate code checkbox")
            public void I_select_promotional_or_corporate_code_checkbox() throws Throwable {
                Log.info("Step Started: I select promotional or corporate code checkbox");
                searchHotelResultPage.clickonpromotioncoderadiobutton();
                Log.info("Step Ended: I select promotional or corporate code checkbox");
            }

            @When("I enter public offer code {string}")
            public void I_enter_public_offer_code(String promotionCode) {
                Log.info("Step Ended: I enter public offer code");
                searchHotelResultPage.enterPromotionalcode(promotionCode);
                Log.info("Step Ended: I enter public offer code");
            }

            @When("I click on discount scandic friends member booking button")
            public void I_click_on_discount_scandic_friends_member_booking_button() throws Throwable {
                Log.info("Step Started: I click on discount scandic friends member booking button");
                utility.verifyTextPresent(roomRateDetailsPage.getBookingcode(),"FG2");
                ScandicFriendsBooking_text=roomRateDetailsPage.getScandicfriendsookingtext()+" "+ roomRateDetailsPage.getScandicFriendMembertext();
                ScandicFriendsBooking_Price=roomRateDetailsPage.getdiscountscandicfriendsbookingPrice();

                //This logic use for multiple nights condition in android
                String[] SF_Price_parts = ScandicFriendsBooking_Price.split(" ");
                String SF_price_number = SF_Price_parts[0];
                SF_price_currency= SF_Price_parts[1];
                SF_price_currency_removenight= SF_price_currency.replace("/Night","");
                SF_price_N=SF_price_number.replaceAll("[-+.^:,]","");
                SF_Price_Value= Integer.parseInt(SF_price_N);
                roomRateDetailsPage.clickonDiscountScandiccodebookbutton();
                Log.info("Step Ended: I click on discount scandic friends member booking button");
            }

            @Then("^I should navigate to scandic friends booking screen$")
            public void I_should_navigate_to_scandic_friends_booking_screen() throws Throwable {
                Log.info("Step Started: I should navigate to scandic friends booking screen");
                utility.verifyTextPresent(confirmBookingPage.getflexconfirmheadertitle(), "Confirm");
                utility.verifyTextPresent(confirmBookingPage.getbestpriceheadertext(), "BEST PRICE GUARANTEED");
                // Verify Price Per Night
                utility.verifyTextPresent(confirmBookingPage.getPriceperNightdetails(), ScandicFriendsBooking_Price+"/Night");
                // Verify Flex booking Text
                utility.verifyTextPresent(confirmBookingPage.getBookingType() , ScandicFriendsBooking_text);
                Log.info("Step Ended: I should navigate to scandic friends booking screen");
            }

            @Then("I click on SF discount book button to complete booking")
            public void I_click_on_SF_discount_book_button_to_complete_booking() throws Throwable {

                Log.info("Step Started: I click on SF discount book button to complete booking");
                int final_Value= SF_Price_Value * confirmBookingPage.Total_Number_Nights();
                String Final_Value = testUtil.adddecimalinamount(final_Value);


                // Verify total price
                if (confirmBookingPage.getnumberofNights().equalsIgnoreCase("1 night")){
                    utility.verifyTextPresent(confirmBookingPage.getPricedetails(), ScandicFriendsBooking_Price);
                    utility.verifyTextPresent(confirmBookingPage.gettotalfinalPrice(),ScandicFriendsBooking_Price);
                    utility.verifyTextPresent(confirmBookingPage.getbottomFinalprice(),ScandicFriendsBooking_Price);

                }
                else{
                    utility.verifyTextPresent(confirmBookingPage.getPricedetails(), ScandicFriendsBooking_Price);
                    utility.verifyTextPresent(confirmBookingPage.gettotalfinalPrice(), Final_Value+" "+SF_price_currency_removenight);

                    utility.verifyTextPresent(confirmBookingPage.getbottomFinalprice(), Final_Value +" "+SF_price_currency_removenight);
                }

                confirmBookingPage.clickonBookbutton();
                Log.info("Step Ended: I click on SF discount book button to complete booking");
            }
        }
