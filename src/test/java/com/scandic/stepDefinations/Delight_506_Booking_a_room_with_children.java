package com.scandic.stepDefinations;

import com.scandic.Testutilities.MobileActions;
import com.scandic.Testutilities.Utility;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Search_Hotel_Result_Page;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Delight_506_Booking_a_room_with_children extends CreateDriver {

    Search_Hotel_Result_Page searchHotelResultPage=new Search_Hotel_Result_Page(getDriver());
    Utility utility=new Utility();

    @When("I click on Guest button")
    public void I_click_on_Guest_button() throws Throwable{

        searchHotelResultPage.clickonguestbutton();

    }

    @Then("Room & Guests modal is opened")
    public void Room_and_Guests_modal_is_opened()throws Throwable  {

        utility.verifyTextPresent(searchHotelResultPage.getroomsguestsModaltext(), "Rooms & Guests");
    }

    @Then("I can select guest six adults")
    public void I_can_select_guest_six_adults ()throws Throwable {
        utility.verifyTextPresent(searchHotelResultPage.getadultcount(),"1");


        for(int i=1;i<=5;i++){
            searchHotelResultPage.clickadultbutton();
        }
        utility.verifyTextPresent(searchHotelResultPage.getadultcount(),"6");

    }

    @Then("I can select guest five child")
    public void I_can_select_guest_five_child() throws Throwable {
        utility.verifyTextPresent(searchHotelResultPage.getchildcount(),"0");
        for(int j=1;j<=5;j++){
            searchHotelResultPage.clickchildbutton();

        }
        utility.verifyTextPresent(searchHotelResultPage.getchildcount(),"5");
    }

    @Then("I changed guest to two adults")
    public void I_changed_guest_to_two_adults() throws Throwable {
        for(int i=1;i<=4;i++){
            searchHotelResultPage.clickremoveadultbutton();
        }
        utility.verifyTextPresent(searchHotelResultPage.getadultcount(),"2");
    }

    @Then("I changed guest to two child")
    public void I_changed_guest_to_two_child() throws Throwable {
        for(int j=1;j<=3;j++){
            searchHotelResultPage.clickremovechildbutton();

        }

    }

    @When("I select first children age is {int} years")
    public void I_select_first_children_age(Integer int1) throws Throwable {
       // getDriver().findElementsByClassName("XCUIElementTypePickerWheel").get(0).sendKeys("11");
        getDriver().findElementsById("v_child_dropdown").get(0).click();
        MobileActions actions=new MobileActions();
        getDriver().findElementByClassName("android.widget.NumberPicker").sendKeys("2");
        //getDriver().findElementsById("v_child_dropdown").get(0).sendKeys("2");
        Thread.sleep(5000);
        getDriver().findElementsById("v_child_dropdown").get(0).sendKeys("2");
        Thread.sleep(5000);
    }

    @Then("I can see bed type is crib")
   public void  I_can_see_bed_type_is_crib() throws Throwable {

    }

    @When("I select second children age is {int} years")
    public void I_select_second_children_age(Integer int1) throws Throwable {

    }
    @Then("I can see bed type is extra bed")
    public void I_can_see_bed_type_is_extra_bed() throws Throwable {

    }

    @When("I select bed type is in adult bed for both the children")
    public void I_select_bed_type_is_in_adult_bed_for_both_the_children() throws Throwable {

    }

    @Then("I changed guest to {int} adult")
    public void I_changed_guest_to_adult (Integer int1) throws Throwable{

    }

    @Then("I can see one of the children bed changes to crib")
    public void I_can_see_one_of_the_children_bed_changes_to_crib() throws Throwable {

    }

    @Then("I can verify guest details {int} adult and {int} children")
    public void I_can_verify_guest_details_adults_child(Integer int1, Integer int2) throws Throwable{

    }

    @When("I click on My Stays Menu tab")
    public void I_click_on_My_Stays_Menu_tab() throws Throwable{

    }

    @Then("I should navigate to My stays screen")
    public void I_should_navigate_to_My_stays_screen() throws Throwable {

    }

}
