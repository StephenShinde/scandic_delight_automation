package com.scandic.stepDefinations;

import java.util.List;

import com.scandic.ExternalUtilities.Log;
import net.bytebuddy.implementation.bytecode.Throw;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.touch.TouchActions;



import com.scandic.Testutilities.Utility;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Launch_Page;
import com.scandic.pages.Signup_Page;


import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TC5_Signup extends CreateDriver {

	Signup_Page signUpPage=new Signup_Page(getDriver());
	Launch_Page launchPage=new Launch_Page(getDriver());
	Utility utility=new Utility();

	@When("^I click on Signup button on launch screen$") 
	public void I_click_on_Signup_button_on_launch_screen() throws Throwable {
		Log.info("Step Started: I click on Signup button on launch screen");
		launchPage.clickonSignupbutton();
		Log.info("Step Ended: I click on Signup button on launch screen");
	}

	@Then("^I should navigate to Signup screen$")
	public void I_should_navigate_to_Signup_screen() throws Throwable {
		Log.info("Step Started: I should navigate to Signup screen");

		utility.waitTillElementsGetVisible(signUpPage.Signup_Page_Title, 30);
		utility.verifyTextPresent(signUpPage.getSignupTitle(), "Join Scandic Friends");

		Log.info("Step Ended: I should navigate to Signup screen");
	}

	@Then("I enter first name as {string}")
	public void I_enter_first_name(String firstName) throws Throwable {
		signUpPage.enterFirstName(firstName);
		utility.hideKeyboard();
	}

	@Then("I enter Last name as {string}")
	public void I_enter_Last_name(String lastName) throws Throwable {
		signUpPage.enterLastName(lastName);
		utility.hideKeyboard();
	}

	@Then("I enter Email as {string}")
	public void I_enter_Email(String email) throws Throwable {
		signUpPage.enterEmail(email);
		utility.hideKeyboard();
	}

	@Then("I enter New password as {string}")
	public void I_enter_New_password(String newPassword) throws Throwable {
		signUpPage.enterNewPassword(newPassword);
		utility.hideKeyboard();
	}

	@Then("I enter Confirm Password as {string}")
	public void I_enter_Confirm_Password(String confirmPassword) throws Throwable {
		signUpPage.enterConfirmPassword(confirmPassword);
		utility.hideKeyboard();
	}

	@Then("I enter Mobile Number as {string}")
	public void I_enter_Mobile_Number(String mobileNumber) throws Throwable {
		if (getDriver().getCapabilities().getCapability("platformName").toString().equalsIgnoreCase("android")) {
			signUpPage.click_Country_code();
			signUpPage.enterMobileNumber(mobileNumber);}
		else {
			signUpPage.enterMobileNumber(mobileNumber);}

			utility.hideKeyboard();

	}



	@Then("^I select Date of birth$")
	public void I_select_Date_of_birth() throws Throwable {
		signUpPage.select_dob();

	}

	@Then("^I select country$") 
	public void I_select_country() throws Throwable {
		signUpPage.select_country();
	}

	@Then("^I click on Terms Conditions checkbox$")
	public void I_click_on_Terms_Conditions_checkbox() throws Throwable {
		signUpPage.click_TC();
	}

	@When("^I Click on Signup button$")
	public void I_Click_on_Signup_button() throws  Throwable {
		signUpPage.click_signup();
	}

	@Then("^I should navigate to signup success screen$")
	public void I_should_navigate_to_signup_success_screen() throws Throwable {
		utility.waitTillElementsGetVisible(signUpPage.Later_link,240);
		utility.verifyTextPresent(signUpPage.get_Thanks_text(), "THANK YOU");
		
	}


	@Then("I click on Later button")
	public void I_click_on_Later_button() throws Throwable {
			signUpPage.click_later();
	}

	@Then("I should navigate to Launch screen")
	public void I_should_navigate_to_Launch_screen() throws Throwable {

		utility.waitTillElementsGetVisible(launchPage.Login_button, 60);
		utility.verifyTextPresent(launchPage.getLogintext(), "Log In");

	}


}
