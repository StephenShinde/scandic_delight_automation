package com.scandic.stepDefinations;
import org.testng.Assert;
import com.scandic.ExternalUtilities.Log;
import com.scandic.Testutilities.Utility;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Calendar_Page;
import com.scandic.pages.Launch_Page;
import com.scandic.pages.Search_Hotel_Page;
import com.scandic.pages.Search_Hotel_Result_Page;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TC3_Search_Hotel_City extends CreateDriver {
	Launch_Page launchpage=new Launch_Page(getDriver());
	Search_Hotel_Page searchhotelpage=new Search_Hotel_Page(getDriver());
	Calendar_Page calendarpage=new Calendar_Page(getDriver());
	Search_Hotel_Result_Page searchhotelresultpage=new Search_Hotel_Result_Page(getDriver());
	Utility utility=new Utility();
	String Search_hotelName;

	@When("^I click on Later link$")
	public void I_click_on_Later_link() throws Throwable {
		Log.info("Step Started: I click on Later link");
		
		launchpage.clickonLaterLink();
		
		Log.info("Step Ended: I click on Later link");
	}

	@When("^I click on search box$")
	public void I_click_on_search_box() throws Throwable {
		Log.info("Step Started: I click on search box");
		
		searchhotelpage.clickonSearchHotelDestination();
		
		Log.info("Step Ended: I click on search box");
	}

	@When("^I enter City name in search box$") 
	public void I_enter_City_name_in_search_box() throws Throwable{
		Log.info("Step Started: I enter City name in search box");
		
		Assert.assertTrue(searchhotelpage.Search_text.isDisplayed());
		searchhotelpage.enterCityName();
		
		Log.info("Step Ended: I enter City name in search box");
	}

	@When("^I click search button$") 
	public void I_click_search_button() throws Throwable {
		Log.info("Step Started: I click search button");
		
		searchhotelpage.getlistname();
		searchhotelpage.selectsearchlistfromlist();
		
		Log.info("Step Ended: I click search button");
	}

	@Then("^I should navigate on calendar screen$") 
	public void I_should_navigate_on_calendar_screen() throws Throwable {
		Log.info("Step Started: I should navigate on calendar screen");
		//utility.verifyTextPresent(calendarpage.getCalenderheadertext(), "Search dates");
		//String calendar_header=getDriver().findElement(By.id("Search dates_label")).getText();
		Log.info("Step Ended: I should navigate on calendar screen");
	}

	@When("^I select check In date$") 
	public void I_select_check_In_date() throws  Throwable {
		Log.info("Step Started: I select check In date");
		
		Log.info("Step Ended: I select check In date");
	}

	@When("I select check out date$")
	public void I_select_check_out_date() throws Throwable {
		Log.info("Step Started: I select check out date");
		
		Log.info("Step Ended: I select check out date");
	}

	@When("^I click on Show Results button$")
	public void I_click_on_Show_Results_button() throws Throwable {
		Log.info("Step Started: I click on Show Results button");
		
		calendarpage.clickonShowResults();
		
		Log.info("Step Ended: I click on Show Results button");
	}

	@Then("^I can see availability hotels in result$")
	public void I_can_see_availability_hotels_in_resul() throws Throwable {
		Log.info("Step Started: I can see availability hotels in result");

		utility.waitTillElementsGetVisible(searchhotelresultpage.hotel_list_name, 240);
		utility.verifyElementPresent(searchhotelresultpage.addguest_button);
		utility.verifyElementPresent(searchhotelresultpage.code_rewar_button);
		Search_hotelName=searchhotelresultpage.getsearchresultHotelName();
		Log.info("Hotel name is "+ Search_hotelName );
		Log.info("Step Ended: I can see availability hotels in result");
	}



}
