package com.scandic.stepDefinations;

import com.scandic.ExternalUtilities.Log;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Search_Hotel_Page;

import io.cucumber.java.en.When;

public class TC4_Search_Hotel_Name extends CreateDriver {

	@When("^I enter hotel name in search box$") 
	public void I_enter_hotel_name_in_search_box() throws Throwable {
		Log.info("Step Started: I enter hotel name in search box");
		
		Search_Hotel_Page searchhotelpage=new Search_Hotel_Page(getDriver());
		searchhotelpage.enterHotelName();
		
		Log.info("Step Ended: I enter hotel name in search box");
	}

}
