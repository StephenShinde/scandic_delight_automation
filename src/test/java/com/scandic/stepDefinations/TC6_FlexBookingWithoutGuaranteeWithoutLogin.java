package com.scandic.stepDefinations;
import com.scandic.ExternalUtilities.Log;
import com.scandic.Testutilities.MobileActions;
import com.scandic.Testutilities.TestUtil;
import com.scandic.Testutilities.Utility;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Confirm_Booking_Page;
import com.scandic.pages.Room_Category_Page;
import com.scandic.pages.Room_Rate_Details_Page;
import com.scandic.pages.Search_Hotel_Result_Page;
import com.scandic.pages.ThankYou_Page;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TC6_FlexBookingWithoutGuaranteeWithoutLogin extends CreateDriver {

	Search_Hotel_Result_Page searchhotelresultpage=new Search_Hotel_Result_Page(getDriver());
	Room_Category_Page roomcategorypage=new Room_Category_Page(getDriver());
	Room_Rate_Details_Page roomratedetailspage=new Room_Rate_Details_Page(getDriver());
	Confirm_Booking_Page confirmbookingpage=new Confirm_Booking_Page(getDriver());
	ThankYou_Page thankyoupage=new ThankYou_Page(getDriver());
	Utility utility=new Utility();
	MobileActions mobileactions=new MobileActions();
	String hotelName;
	String HotelPrice;
	String roomcategoryType;
	String hoteladdress;
	String roomarea;
	String guestCapacity;
	String FlexBookingPrice;
	String Flexbookingtext;
	String FlexCurrency;
	String Flex_Price;
	String Flex_Priceprnight;
	int Flex_Price_Value;
	String Flex_price_currency;
	String Flex_proce_N;
	String Flex_price_currency_removenight;



	@When("^I click on any availble hotel$")
	public void I_click_on_any_availble_hotel() throws Throwable {
		Log.info("Step Started: I click on any availble hotel");
		
		utility.waitTillElementsGetVisible(searchhotelresultpage.hotel_list_name, 240);
		hotelName=searchhotelresultpage.getsearchresultHotelName();
		HotelPrice=searchhotelresultpage.getsearchresultHotelPrice();
		searchhotelresultpage.clickonSearchresulthotel();
		
		Log.info("Step Ended: I click on any availble hotel");
	}

	@Then("^I should navigate to room category details screen$")
	public void I_should_navigate_to_room_category_details_screen() throws Throwable {
		Log.info("Step Started: I should navigate to room category details screen");
		
	//	utility.verifyTextPresent(roomcategorypage.getHotelName(),hotelName );
		utility.verifyElementPresent(roomcategorypage.about_hotel);
		//utility.verifyTextPresent(roomcategorypage.getRoomprice(),"From "+HotelPrice);
		hoteladdress=roomcategorypage.getAddress();
		roomcategoryType= roomcategorypage.getRoomtypeName();
		roomarea=roomcategorypage.getroomArea();
		guestCapacity=roomcategorypage.getroomcapacityGuest();
		
		Log.info("Step Ended: I should navigate to room category details screen");
	}

	@When("^I click on room category type$")
	public void I_click_on_room_category_type() throws Throwable {
		Log.info("Step Started: I should navigate to room category details screen");
		
		roomcategorypage.clickonRoomtype();
		
		Log.info("Step Ended: I should navigate to room category details screen");
	}

	@Then("^I should navigate room rate details screen$")
	public void I_should_navigate_room_rate_details_screen() throws Throwable {
		Log.info("Step Started: I should navigate room rate details screen");

		// Verify Booking Room Type
		utility.verifyTextPresent(roomratedetailspage.getRoomtypeName(), roomcategoryType);
		// Verify Booking Room Area
		utility.verifyTextPresent(roomratedetailspage.getRoomarea(), roomarea);
		// Verify room guest capacity
		utility.verifyTextPresent(roomratedetailspage.getguestcapacity(), guestCapacity);
		
		Log.info("Step Ended: I should navigate room rate details screen");
	}

	@When("^I click on flex booking button$") 
	public void I_click_on_flex_booking_button() throws Throwable {
		Log.info("Step Started: I click on flex booking button");
		
		Flexbookingtext= roomratedetailspage.getFlexbookingtext();
		Flex_Price=roomratedetailspage.getFlexbookingPrice();

		String[] Flex_Price_parts = Flex_Price.split(" ");
		String Flex_price_number = Flex_Price_parts[0];
		Flex_price_currency= Flex_Price_parts[1];
		Flex_price_currency_removenight= Flex_price_currency.replace("/Night","");
		Flex_proce_N=Flex_price_number.replaceAll("[-+.^:,]","");

		Flex_Price_Value= Integer.parseInt(Flex_proce_N);

		roomratedetailspage.clickonFlexbooking();
		
		Log.info("Step Ended: I click on flex booking button");
	}

	@Then("^I should navigate to confirm Flex booking screen$")
	public void I_should_navigate_to_confirm_flex_booking_screen() throws Throwable {
		Log.info("Step Started: I should navigate to confirm Flex booking screen");
		
		utility.verifyTextPresent(confirmbookingpage.getflexconfirmheadertitle(), "Confirm");
		utility.verifyTextPresent(confirmbookingpage.getbestpriceheadertext(), "BEST PRICE GUARANTEED");
		// Verify Room Category Type
		utility.verifyTextPresent(confirmbookingpage.getRoomtitle(), roomcategoryType);
		// Verify Price Per Night
		//Temporary Comment
		utility.verifyTextPresent(confirmbookingpage.getPriceperNightdetails(), Flex_Price+"/Night");
		// Verify Flex booking Text
		utility.verifyTextPresent(confirmbookingpage.getBookingType(), Flexbookingtext);
		
		Log.info("Step Ended: I should navigate to confirm Flex booking screen");
		
		
	}

	@And("^I enter contact information$")
	public void I_enter_contact_information() throws Throwable{
		Log.info("Step Started: I enter contact information");
		
		confirmbookingpage.clickoncontactInfobox();
		confirmbookingpage.enterBookingInformation("TestName", "LastName", "email@email.com", "769845231");
		confirmbookingpage.clickonsmsConfirmationCheckbox();
		confirmbookingpage.savebutton();
		
		Log.info("Step Ended: I enter contact information");
	}

	@Then("^I click on termsandcondition checkbox$") 
	public void I_click_on_termsandcondition_checkbox() throws Throwable {
		Log.info("Step Started: I click on termsandcondition checkbox");
		
		//Verify Hotel Name
		mobileactions.ScrollToexactElement(confirmbookingpage.bottom_price_final, 0.7, 0.3, 0.2);
		utility.verifyTextPresent(confirmbookingpage.getHotelname(), hotelName);
		// Verify Hotel Address
		utility.verifyTextPresent(confirmbookingpage.getHoteladdress(), hoteladdress);
		confirmbookingpage.clickonstermsconditionConfirmationCheckbox();
		
		Log.info("Step Ended: I click on termsandcondition checkbox");
	}


	@Then("^I click on book button to complete booking$") 
	public void I_click_on_book_button_to_complete_booking() throws Throwable{

		Log.info("Step Started: I click on book button to complete booking");
		int final_Value= Flex_Price_Value * confirmbookingpage.Total_Number_Nights();
		TestUtil testUtil=new TestUtil();
		String Final_Value = testUtil.adddecimalinamount(final_Value);

		String Total_Flex_price = String.valueOf(final_Value);
		// Verify total price
		if (confirmbookingpage.getnumberofNights().equalsIgnoreCase("1 night")){
			utility.verifyTextPresent(confirmbookingpage.getPricedetails(), Flex_Price);
			utility.verifyTextPresent(confirmbookingpage.gettotalfinalPrice(),Flex_Price);
			utility.verifyTextPresent(confirmbookingpage.getbottomFinalprice(),Flex_Price);

		}
		else{
			utility.verifyTextPresent(confirmbookingpage.getPricedetails(), Flex_Price);
			utility.verifyTextPresent(confirmbookingpage.gettotalfinalPrice(), Final_Value+" "+Flex_price_currency_removenight);

			utility.verifyTextPresent(confirmbookingpage.getbottomFinalprice(), Final_Value +" "+Flex_price_currency_removenight);
		}




		confirmbookingpage.clickonBookbutton();

		
		Log.info("Step Ended: I click on book button to complete booking");
	}

	@Then("^I should navigate to Thank You screen$") 
	public void I_should_navigate_to_Thank_You_screen() throws Throwable{
		Log.info("Step Started: I should navigate to Thank You screen");
		
		utility.waitTillElementsGetVisible(thankyoupage.thankyou_text, 240);
		utility.verifyTextPresent(thankyoupage.getthankyouText(), "THANK YOU");
		utility.verifyTextPresent(thankyoupage.getbookingconfirmationtext(), "YOUR BOOKING HAS BEEN ADDED TO MY STAYS");

		
		Log.info("Step Ended: I should navigate to Thank You screen");
	}

	@Then("^I click on done button$") 
	public void I_click_on_done_button() throws Throwable{
		Log.info("Step Started: I click on done button");
		
		thankyoupage.clickonDonebutton();
		
		Log.info("Step Ended: I click on done button");
	}

}
