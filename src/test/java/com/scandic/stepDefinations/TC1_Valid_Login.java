package com.scandic.stepDefinations;
import com.scandic.ExternalUtilities.Log;
import com.scandic.Testutilities.MobileActions;
import com.scandic.Testutilities.Utility;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Launch_Page;
import com.scandic.pages.Login_Page;
import com.scandic.pages.My_Profile_Page;
import com.scandic.pages.Search_Hotel_Page;
import io.appium.java_client.MobileBy;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TC1_Valid_Login extends CreateDriver {
	Launch_Page launchpage=new Launch_Page(getDriver());
	Utility utility=new Utility();
	Login_Page loginpage=new Login_Page(getDriver());
	My_Profile_Page myprofilepage=new My_Profile_Page(getDriver());
	Search_Hotel_Page searchhotelpage=new Search_Hotel_Page(getDriver());
	MobileActions mobileactions=new MobileActions();

	@Given("Application is launched")
	public void Application_is_launched() throws Throwable {
		Log.info("Step Started: Application is launched");
		Log.info("Wait till login button is visible");
		utility.waitTillElementsGetVisible(launchpage.Login_button, 60);
		Log.info("Verify Log In text is present");
		utility.verifyTextPresent(launchpage.getLogintext(), "Log In");
		Log.info("Step Ended: Application is launched");
	}

	@When("I click on Login button on launch screen") 
	public void I_click_on_Login_buttonon_launch_screen() throws Throwable {
		Log.info("Step Started: I click on Login button on launch screen");
		Log.info("Click on login button");
		launchpage.clickonLoginbutton();

		if(getDriver().getCapabilities().getCapability("platformName").toString().equalsIgnoreCase("ios")) {
			loginpage.clickonContinuePopup();
			
			Log.info("Step Ended: I click on Login button on launch screen");
		}
		
	}
	
	

//	@Then("I should navigate to Login screen")
//	public void  I_should_navigate_to_Login_screen() throws Throwable {
//		Log.info("Step Started: I should navigate to Login screen");
//		Log.info("Wait till login banner visible");
//		utility.waitTillElementsGetVisible(loginpage.Loggin_banner, 30);
//		Log.info("Verify loggin banner element is visible");
//		utility.verifyElementPresent(loginpage.Loggin_banner);
//		Log.info("Step Ended: I should navigate to Login screen");
//
//	}

	@When("I enter valid email and valid password") 
	public void I_enter_valid_email_and_valid_password() throws Throwable {	
		Log.info("Step Started: I enter valid email and valid password");
		Log.info("Click on email field");
		loginpage.click_email();
		Log.info("Clear email field");
		loginpage.clearEmailfield();
		getDriver().hideKeyboard();
		String Emailfield=	loginpage.Email.get(0).getText();
		System.out.println(Emailfield);
		loginpage.clearEmailfield();
		Log.info("Enter emailid");
		loginpage.enter_email("scandicapps+cindy@gmail.com");
		String platform=getDriver().getCapabilities().getCapability("platformName").toString();
		if(getDriver().getCapabilities().getCapability("platformName").toString().equalsIgnoreCase("ios")) {
			Log.info("Click on iOS password field");
			loginpage.click_ios_password();
			Log.info("Enter password for iOS");
			loginpage.enter_ios_password("scandic123");
		}
		else {
			Log.info("Click on android password field");
			loginpage.click_android_password();
			Log.info("Enter password for android");
			loginpage.enter_android_password("scandic1234");
		}
		Log.info("Step Ended: I enter valid email and valid password");
	}

	@When("I click on Login button")
	public void I_click_on_Login_button() throws Throwable {
		Log.info("Step Started: I click on Login button");
		Log.info("Click on login button");
		loginpage.click_Login();
		Log.info("Step Ended: I click on Login button");
		}

	@Then("I should navigate to Book Hotel screen") 
	public void I_should_navigate_to_Book_Hotel_screen() throws Throwable {
		Log.info("Step Started: I should navigate to Book Hotel screen");
		Log.info("Verify book hotel menu");
		utility.verifyElementPresent(searchhotelpage.Book_Hotel_Menu);
		Log.info("Step Ended: I should navigate to Book Hotel screen");
	}

	@When("I click on My Profile menu") 
	public void I_click_on_My_Profile_menu() throws Throwable {
		Log.info("Step Started: I click on My Profile menu");
		Log.info("Click on my profile menu");
		myprofilepage.clickonmyProfilemenu();
		Log.info("Step Ended: I click on My Profile menu");
	}

	@Then("I should navigate on My Profile Screen") 
	public void I_should_navigate_on_My_Profile_Screen() throws Throwable{
		Log.info("Step Started: I should navigate on My Profile Screen");
		Log.info("");
		utility.verifyElementPresent(myprofilepage.My_Profile_menu);
		Log.info("Step Ended: I should navigate on My Profile Screen");
	}

	@When("I click on Logout button") 
	public void I_click_on_Logout_button() throws Throwable{
		Log.info("Step Started: I click on Logout button");

		mobileactions.ScrollToexactElementandClick(myprofilepage.Logout, 0.7, 0.3, 0.5);
		myprofilepage.clickOnLogout();
		myprofilepage.popUp_logout.click();
		Log.info("Step Ended: I click on Logout button");

	}

	@Then("I should Logout from the application") 
	public void I_should_Logout_from_the_application() throws Throwable {
		Log.info("Step Started: I should Logout from the application");
		utility.verifyElementPresent(launchpage.Login_button);
		utility.verifyElementPresent(launchpage.Later_link);
		utility.verifyElementPresent(launchpage.Signup_button);
		Log.info("Step Ended: I should Logout from the application");
	}

}
