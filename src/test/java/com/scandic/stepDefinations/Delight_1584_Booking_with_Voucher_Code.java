package com.scandic.stepDefinations;

import com.scandic.ExternalUtilities.Log;
import com.scandic.Testutilities.Utility;
import com.scandic.base.CreateDriver;
import com.scandic.pages.Confirm_Booking_Page;
import com.scandic.pages.Room_Rate_Details_Page;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Delight_1584_Booking_with_Voucher_Code extends CreateDriver {

    Room_Rate_Details_Page roomratedetailspage=new Room_Rate_Details_Page(getDriver());
    Confirm_Booking_Page confirmbookingpage=new Confirm_Booking_Page(getDriver());
    Utility utility=new Utility();
    String GiftVoucher_Booking_text;
    String Gift_Voucher_Price;
    int Gift_Voucher_Price_Value;


    @When("I click on gift voucher booking button")
    public void I_click_on_gift_voucher_booking_button() throws Throwable {
        utility.verifyTextPresent(roomratedetailspage.getBookingcode(),"VOG");
        GiftVoucher_Booking_text=roomratedetailspage.getGiftVouchertext();
        Gift_Voucher_Price=roomratedetailspage.getGift_Voucherprice();
        String[] GiftVoucherparts = Gift_Voucher_Price.split(" ");
        String gift_voucher_price_number = GiftVoucherparts[0];
       Gift_Voucher_Price_Value= Integer.parseInt(gift_voucher_price_number);
        roomratedetailspage.clickonGiftVoucherBookbutton();
        Thread.sleep(5000);
    }

    @Then("I should navigate to confirm gift voucher booking screen")
    public void I_should_navigate_to_confirm_gift_voucher_booking_screen() throws Throwable {
        Log.info("Step Started: I should navigate to confirm gift voucher booking screen");

        utility.verifyTextPresent(confirmbookingpage.getflexconfirmheadertitle(), "Confirm");
        utility.verifyTextPresent(confirmbookingpage.getbestpriceheadertext(), "BEST PRICE GUARANTEED");
        // Verify Room Category Type
        // Commenting this in iOS wrong value passes
        // Verify Price Per Night
        utility.verifyTextPresent(confirmbookingpage.getPriceperNightdetails(), Gift_Voucher_Price+"/Night");
        // Verify Flex booking Text
        utility.verifyTextPresent(confirmbookingpage.getBookingType() , GiftVoucher_Booking_text);

        Log.info("Step Ended: I should navigate to confirm gift voucher booking screen");

    }

    @Then("^I click on voucher book button to complete booking$")
    public void I_click_on_voucher_book_button_to_complete_booking() throws Throwable{
        Log.info("Step Started: I click on voucher book button to complete booking");
        // Verify total price
        if (confirmbookingpage.getnumberofNights().equalsIgnoreCase("1 night")){
            utility.verifyTextPresent(confirmbookingpage.getPricedetails(), Gift_Voucher_Price);
        }
        else{
            utility.verifyTextPresent(confirmbookingpage.getPricedetails(), Gift_Voucher_Price+"/Night");
        }

      //  Gift_Voucher_Price_Value
        int final_Value= Gift_Voucher_Price_Value * confirmbookingpage.Total_Number_Nights();

        String Total_GiftVoucher_price = String.valueOf(final_Value);

        utility.verifyTextPresent(confirmbookingpage.gettotalfinalPrice(), Total_GiftVoucher_price + " "+ "Voucher");

       utility.verifyTextPresent(confirmbookingpage.getbottomFinalprice(), Total_GiftVoucher_price+ " "+ "Voucher");


        confirmbookingpage.clickonBookbutton();

        Log.info("Step Ended: I click on voucher book button to complete booking");
    }

}
