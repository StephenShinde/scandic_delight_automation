package com.scandic.runner;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import com.scandic.ExternalUtilities.Log;
import com.scandic.base.CreateDriver;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/resources/Features", 
glue = { "com.scandic.stepDefinations" },
tags = { "RegressionTest" },
plugin = { "pretty","html:target/cucumber-pretty", "json:target/cucumber-json/cucumber.json","com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"})

public class TestRunner extends AbstractTestNGCucumberTests {
	


	@BeforeMethod
	@Parameters({ "platform_name", "platformVersion", "deviceName", "UDID", "port", "app", "automation_Name" })
	public void initializeApp(String platform_name, String version, String deviceName, String UDID, int port,
			String app, String automation_Name) throws Exception {

		CreateDriver createdriver = new CreateDriver();
		createdriver.setUp(platform_name, version, deviceName, UDID, port, app, automation_Name);
		Log.info("Initialized desired capabilities");
		
	}

	@AfterMethod
	public void closeApp() {
		CreateDriver createdriver = new CreateDriver();
		createdriver.destroyApp();
		Log.info("Close application");
	}

	
	

}
