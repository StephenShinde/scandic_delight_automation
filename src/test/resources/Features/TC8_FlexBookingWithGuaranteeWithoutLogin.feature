#Author: stephen.shinde@scandichotels.com
@RegressionTest
Feature: Flex booking with guarantee entry for single room and single person without login
   As a without login user, I want to make flex booking with guarantee entry for single room and single person


  @TC8_FlexBookingWithGuaranteeWithoutLogin
  Scenario: Flex Booking With Guarantee Entry Without Login
    Given Application is launched
    When I click on Later link
    Then I should navigate to Book Hotel screen
    When I click on search box
    Then I enter hotel name in search box
    And I click search button
    Then I should navigate on calendar screen
    When I select check In date
    And I select check out date
    When I click on Show Results button
    Then I can see availability hotels in result
    When I click on any availble hotel
    Then I should navigate to room category details screen
    When I click on room category type
    Then I should navigate room rate details screen
    When I click on flex booking button
    Then I should navigate to confirm Flex booking screen
    And I enter contact information
    When I click on guarantee your booking checkbox
    Then I should see guarantee your booking checkbox checked
    #And Scroll to Last Element
    And I click on termsandcondition checkbox
    Then I click on book button to complete booking
    And I should navigate to netspayment screen
    When I enter card details
    And I click on register button
    And I should navigate to Thank You screen
    When I click on done button
    Then I should navigate to Book Hotel screen
