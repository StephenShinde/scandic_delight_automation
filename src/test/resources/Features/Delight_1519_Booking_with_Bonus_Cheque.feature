    @RegressionTest
    Feature: Booking with Bonus Cheque
      As a user I would like to be able to book room/s with Bonus cheque/s.Once booked, verify the reservation is correctly showing in My Stays.

      @Deligfht_1519
      Scenario: Booking with Bonus Cheque
        Given Application is launched
        When I click on Login button on launch screen
        When I enter valid email and valid password
        And I click on Login button
        Then I should navigate to Book Hotel screen
        When I click on search box
        Then I enter City name "Stockholm" in search box
        And I click search button
        Then I should navigate on calendar screen
        When I select check In date
        And I select check out date
        And I click on Show Results button
        When I click on codes and rewards nights
        Then I can see booking options modal
        And I select bonus cheque checkbox
        And I click on Show Results button
        Then I can see availability hotels in result
        When I click on any availble hotel
        Then I should navigate to room category details screen
        When I click on room category type
        Then I should navigate room rate details screen
        When I click on bonus cheque booking button
        Then I should navigate to confirm bonus cheque booking screen
        And I click on termsandcondition checkbox
       Then I click on bonus cheque book button to complete booking
        And I should navigate to Thank You screen