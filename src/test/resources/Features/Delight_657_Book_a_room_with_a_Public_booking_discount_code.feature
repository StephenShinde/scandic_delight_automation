  @RegressionTest
  Feature: Book a room with a Public booking discount code
    As a user I would like to book a room with a public discount code and get discount, for example booking code ICA

    @Delight_657
    Scenario: Book a room with a Public booking discount code
      Given Application is launched
      When I click on Login button on launch screen
      When I enter valid email and valid password
      And I click on Login button
      Then I should navigate to Book Hotel screen
      When I click on search box
      Then I enter City name "Stockholm" in search box
      And I click search button
      Then I should navigate on calendar screen
      When I select check In date
      And I select check out date
      And I click on Show Results button
      When I click on Guest button
      Then Room & Guests modal is opened
      When I can select two adults
      And I can Select two child
      And I can select age of the children
      And I click on Show Results button
      When I click on codes and rewards nights
      Then I can see booking options modal
      And I select promotional or corporate code checkbox
      When I enter public offer code "ICA"
      And I click on Show Results button
      Then I can see availability hotels in result
      When I click on any availble hotel
      Then I should navigate to room category details screen
      When I click on room category type
      Then I should navigate room rate details screen

