  #Author: stephen.shinde@scandichotels.com
  @RegressionTest
  Feature: Search for available hotels in a city
     As a user, I want to search hotel by city name

    @Delight_605
    Scenario: Search hotel by city name
      Given Application is launched
      When I click on Later link
      Then I should navigate to Book Hotel screen
      When I click on search box
      Then I enter City name in search box
      And I click search button
      Then I should navigate on calendar screen
      When I select check In date
      And I select check out date
      When I click on Show Results button
      Then I can see availability hotels in result
  #    When I click on My Profile menu
  #    Then I should navigate on My Profile Screen
  #    When I click on Logout button
  #    Then I should Logout from the application
