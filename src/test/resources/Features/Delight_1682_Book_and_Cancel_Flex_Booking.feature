  @RegressionTest
  Feature: Book and Cancel Flex Booking
    As a user I would like to book a FLEX room that you do not need to pay for in advance (until 18:00 the same day) & cancel the booking without any charge

    @Delight_1682
    Scenario: Book and Cancel Flex Booking
      Given Application is launched
      When I click on Login button on launch screen
      When I enter valid email and valid password
      And I click on Login button
      Then I should navigate to Book Hotel screen
      When I click on search box
      Then I enter City name in search box
      And I click search button
      Then I should navigate on calendar screen
      When I select check In date
      And I select check out date
      And I click on Show Results button
      When I click on Guest button
      Then Room & Guests modal is opened
      When I can select two adults
      And I can Select two child
      And I can select age of the children
      When I click on Show Results button
      Then I can see availability hotels in result
      When I click on any availble hotel
      Then I should navigate to room category details screen
      When I click on room category type
      Then I should navigate room rate details screen
      When I click on flex booking button
      Then I should navigate to confirm Flex booking screen
      And I click on termsandcondition checkbox
      Then I click on book button to complete booking
      And I should navigate to Thank You screen
      When I click on done button
      Then I should navigate to Book Hotel screen
  #    When I click on My Stays Menu tab
  #    Then I should navigate to My stays screen
