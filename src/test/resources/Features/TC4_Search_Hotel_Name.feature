  #Author: stephen.shinde@scandichotels.com
  @RegressionTest
  Feature: Search Hotel name feature
     As a user, I want to search hotel by hotel name

    @TC4_Search_Hotel_Name
    Scenario: Search hotel by hotel name
      Given Application is launched
      When I click on Later link
      Then I should navigate to Book Hotel screen
      When I click on search box
      Then I enter hotel name in search box
      And I click search button
      Then I should navigate on calendar screen
      When I select check In date
      And I select check out date
      When I click on Show Results button
      Then I can see availability hotels in result
