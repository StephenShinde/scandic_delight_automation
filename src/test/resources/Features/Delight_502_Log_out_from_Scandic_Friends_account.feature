  #Author: stephen.shinde@scandichotels.com
  @RegressionTest
  Feature: Log out from Scandic Friends account
     As a user I want to be able to Log out from my Scandic Friends account

    @Delight_502
    Scenario: Log out from Scandic Friends account
      Given Application is launched
      When I click on Login button on launch screen
      When I enter valid email and valid password
      And I click on Login button
      Then I should navigate to Book Hotel screen
      When I click on My Profile menu
      Then I should navigate on My Profile Screen
      When I click on Logout button
      Then I should Logout from the application
