    #Author: your.email@your.domain.com
    @Delight_506
    Feature: Booking a room with children
      I want to use this template for my feature file

      @tag1
      Scenario: Booking a room with children
        Given Application is launched
        When I click on Login button on launch screen
        When I enter valid email and valid password
        And I click on Login button
        Then I should navigate to Book Hotel screen
        When I click on search box
        Then I enter hotel name in search box
        And I click search button
        Then I should navigate on calendar screen
        When I select check In date
        And I select check out date
        And I click on Show Results button
        Then I can see availability hotels in result
        When I click on Guest button
        Then Room & Guests modal is opened
        And I can select guest six adults
        And I can select guest five child
        Then I changed guest to two adults
        And I changed guest to two child
        When I select first children age is 2 years
        Then I can see bed type is crib
        When I select second children age is 5 years
        Then I can see bed type is extra bed
        When I select bed type is in adult bed for both the children
        Then I changed guest to 1 adult
        And I can see one of the children bed changes to crib
        When I click on Show Results button
        Then I can see availability hotels in result
        When I click on any availble hotel
        Then I should navigate to room category details screen
        When I click on room category type
        Then I should navigate room rate details screen
        When I click on flex booking button
        Then I should navigate to confirm Flex booking screen
        And I can verify guest details 1 adult and 2 children
        And I click on termsandcondition checkbox
        Then I click on book button to complete booking
        And I should navigate to Thank You screen
        When I click on done button
        Then I should navigate to Book Hotel screen
        When I click on My Stays Menu tab
        Then I should navigate to My stays screen
