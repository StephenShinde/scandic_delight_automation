  #Author: stephen.shinde@scandichotels.com
  @@RegressionTest
  Feature: Create a Scandic Friends account
    As a new member I would like to create a Scandic Friends account

    @Delight_664_test1
    Scenario Outline: : Create a Scandic Friends account
      Given Application is launched
      When I click on Signup button on launch screen
      Then I should navigate to Signup screen
      And I enter first name as "<FirstName>"
      And I enter Last name as "<LastName>"
      And I enter Email as "<Email>"
      And I enter New password as "<NewPassword>"
      And I enter Confirm Password as "<ConfirmPassword>"
      And I enter Mobile Number as "<MobileNumber>"
      And I select Date of birth
      And I select country
      And I click on Terms Conditions checkbox
      When I Click on Signup button
      Then I should navigate to signup success screen
      When I click on Later button
      Then I should navigate to Launch screen

      Examples:
        | FirstName | LastName | Email | NewPassword | ConfirmPassword | MobileNumber |
        | TestUser | Scandic | scandicapp+autotest4@gmail.com | scandic123 | scandic123 | 769545591 |