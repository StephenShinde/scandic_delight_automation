#Author: stephen.shinde@scandichotels.com

@RegressionTest
Feature: Save booking without Login feature
 	As a Scandic user,I want to book hotel room with save booking option and without login

  @TC9_SaveBookingwithoutLogin
  Scenario: Save Booking Without Login
    Given Application is launched
    When I click on Later link
    Then I should navigate to Book Hotel screen
    When I click on search box
    Then I enter hotel name in search box
    And I click search button
    Then I should navigate on calendar screen
    When I select check In date
    And I select check out date
    When I click on Show Results button
    Then I can see availability hotels in result
    When I click on any availble hotel
    Then I should navigate to room category details screen
    When I click on room category type
    Then I should navigate room rate details screen
    When I click on Save booking button
    Then I should navigate to confirm Save booking screen
    And I enter contact information
    And I click on termsandcondition checkbox
     Then I click on Pay with card button to complete booking
    And I click on Pay button
    And I should navigate to Thank You screen
    When I click on done button
    Then I should navigate to Book Hotel screen
    