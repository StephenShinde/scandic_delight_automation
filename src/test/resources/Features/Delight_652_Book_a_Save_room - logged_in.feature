  #Author: stephen.shinde@scandichotels.com

  @RegressionTest
  Feature: Book a Save room - logged in
      As a user I would like to book a SAVE room and pay for it.

    @Delight_652
    Scenario: Book a Save room - logged in
      Given Application is launched
      When I click on Login button on launch screen
      When I enter valid email and valid password
      And I click on Login button
      Then I should navigate to Book Hotel screen
      When I click on search box
      Then I enter hotel name in search box
      And I click search button
      Then I should navigate on calendar screen
      When I select check In date
      And I select check out date
      When I click on Show Results button
      Then I can see availability hotels in result
      When I click on any availble hotel
      Then I should navigate to room category details screen
      When I click on room category type
      Then I should navigate room rate details screen
      When I click on Save booking button
      Then I should navigate to confirm Save booking screen
      And I click on termsandcondition checkbox
      Then I click on Pay with card button to complete booking
      And I click on Pay button
      And I should navigate to Thank You screen
      When I click on done button
      Then I should navigate to Book Hotel screen
